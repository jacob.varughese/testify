# Testify
Testify is a predominately python based tool to configure, generate and execute tests, after which will analysis the results and produce a report.
![](readme/images/intro.png)

## Environment Configuration
Before starting the test, the test environment needs to be set up.
Some of these settings are information only, while others are required. however it is advised that the configuration is fully populated for traceability.

**Required**
 - The inverter project selection will determine the maximum allowed voltages, torques and speed based on the profiles found in `program\src\profiles.py`
 - Dyno location is to determine the configuration of the dyno, but also the configuration of the DCDC (Single or Parallel)

```python
            if DYNO_LOCATION == 'DX: 100kW':
                dcdc = mpy.SingleDCDC(
                    munchInterop = munch_interop,
                    canDevice = DCDCCanDevice
                )
                
            else:
                dcdc = mpy.ParallelDCDC(
                    munchInterop=munch_interop,
                    canDevice=DCDCCanDevice
                )
```

- Selecting the correct Transducer is also required as they both use differnent DDLs and methods.
- The correct CAN HWID's must be selected, otherwise the test scripts can not identify what hardware is on what dongle.

```python
            MCUCanDevice = mpy.CANDevice.getSpecific(
                munchInterop=munch_interop,
                name=CAN_HWID_MCU
            )

            DCDCCanDevice = mpy.CANDevice.getSpecific(
                munchInterop=munch_interop,
                name=CAN_HWID_DCDC
            )
```

- It is recommened to request the MCU to login, this will allow the scripts to download properties from the inverter. Some scripts, such as Idq injection, need to login in order write to specific properties.
  
![](readme/images/enviroment.png)
## Test Configuration
To switch between the test scripts, the dropdown `Test Type` is used.
This will determine the rest of the configuration options below, which are unique to the test.

![](readme/images/test.png)

## Scripts
Python scripts are used to automate the test environment using the test configuration parameters and generated test points.

There are currently the following scripts available for use with Testify:
- Idq Characterisation
- Idq Injection
- Torque Speed Sweep

However, the following are planned:
- Encoder Auto-Alignment
- Latency Table Auto-Tune
- "Custom" - Add line by line to build a test, that will execute.. line by line.
- Idq Characterisation : Power Analyser
- QT Characterisation

### Idq Characterisation
#### Introduction
The Idq characterisation script uses the `Sevcon` traditional method of calculating motor parameters.
The motor is spun to a set speed (normally 500rpm), and known currents are injected in the stationary reference frame.

===should latency tables be zero?===

## Results

### Idq Characterisation



### Torque Speed Sweep




