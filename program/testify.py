import os
from datetime import datetime
import pandas as pd
import streamlit as st
from src.ui import ui_profile, ui_hw, ui_cfg, ui_tester, ui_plots
from src import generator as gen
from src import report_gen
from src import constants

# OPEN CMD: streamlit run <filenames>.py
# https://pep8.org/
# python -m autopep8 --max-line-length=130 -i

st.set_page_config(
    layout="wide",
    page_title="Testify",
    page_icon="📈",
    initial_sidebar_state='expanded'
)

# Initialise
datetime_format = ""
datetime_now = datetime.now()
format = "%Y_%m_%d_%H_%M_%S_"
datetime_format = datetime_now.strftime(format)
test_name = constants.FIELD_INVERTER_NAME
mcu_login = False

paths = {
    "script": os.getcwd() + r"\test_script.py",
    "idq script": (os.getcwd() + "\\" + r"program\src\scripts\idq_script.py"),
    "torque speed script": (os.getcwd() + "\\" + r"program\src\scripts\torque_speed_script.py"),
    "log": os.getcwd() + r"\console_out.log",
    "logo": (os.getcwd() + r"\program\src\images\logo_turntide.png"),
    "export": (os.getcwd() + "\\")
}

test_points = pd.DataFrame(
    data={
        "torque_demand": [],
        "demand_period": [],
        "speed_demand": [],
        "speed_demand_rads": [],
        "speed_lim_fwd": [],
        "speed_lim_rev": [],
        "power_mech": []
    })

envolope = pd.DataFrame(data={"torque_envelope": [], "speed_envelope": []})

#UI

st.markdown("`v1.0`")

l_col, r_col = st.columns(2)

profile_tab, hw, cfg, debug = l_col.tabs(["Profile", "Hardware", "Test Configuration", "Debug"])

test_dict = ui_profile.tab_profile(profile_tab)
hw_dict = ui_hw.tab_hw(hw)
cfg_dict = ui_cfg.tab_cfg(cfg, hw_dict, test_dict)

if test_dict["Debug"] is True:
    debug.markdown("## Paths")
    debug.write(paths)
else:
    debug.info("Tool Debug not enabled, enable in Profile Tab.")

if test_dict["Type"] in ["Idq Injection", "Idq Characterisation"]:

    gen_cfg = {
        "Test": {
            "Type": test_dict["Type"],
            "Profile": test_dict["Profile"]
        },
        "Voltage": {
            "Voltage Breakpoints": hw_dict["MCU"]["Voltage Breakpoints"]
        },
        "Idq": {
            "Maximum": cfg_dict["Idq"]["Maximum"],
            "Step": cfg_dict["Idq"]["Step"],
            "Step Method": cfg_dict["Idq"]["Step Method"],
            "Step Period": cfg_dict["Idq"]["Step Period"],
            "Ramp Period": cfg_dict["Idq"]["Step Period"],
            "Iq Direction": None
        },
        "Speed": {
            "Target": cfg_dict["Speed"]["Target"],
            "Limit": cfg_dict["Speed"]["Limit Value"],
            "Max Table": hw_dict["MCU"]["Max Speed"],
            "Breakpoints": hw_dict["MCU"]["Speed Breakpoints"],
            "Direction": None
        }
    }

    test_points = gen.idq_table(gen_cfg)
    
    if test_dict["Debug"] is True:
        debug.markdown("## Generator Config")
        debug.write(gen_cfg)
        debug.markdown("## Test points")
        debug.write(test_points)

else:

    gen_cfg = {
        "Test": {
            "Type": test_dict["Type"],
            "Profile": test_dict["Profile"]
        },
        "Voltage": {
            "Breakpoints": hw_dict["MCU"]["Voltage Breakpoints"],
            "Voltage": hw_dict["DCDC"]["Voltage"]
        },
        "Torque": {
            "Peak": hw_dict["MCU"]["Peak Torque"],
            "Maximum": cfg_dict["Torque"]["Maximum"],
            "Minimum": cfg_dict["Torque"]["Minimum"],
            "Ramp Up Value": cfg_dict["Torque"]["Ramp Up Torque"],
            "Ramp Down Value": cfg_dict["Torque"]["Ramp Down Torque"],
            "Ramp Up Period": cfg_dict["Torque"]["Ramp Up Period"],
            "Ramp Down Period": cfg_dict["Torque"]["Ramp Down Period"],
            "Skip": cfg_dict["Torque"]["Skip"]
        },
        "Speed": {
            "Max Table": hw_dict["MCU"]["Max Speed"],
            "Breakpoints": hw_dict["MCU"]["Speed Breakpoints"],
            "Maximum": cfg_dict["Speed"]["Maximum"],
            "Minimum": cfg_dict["Speed"]["Minimum"],
            "Step": cfg_dict["Speed"]["Step"],
            "Limit Type": cfg_dict["Speed"]["Limit Type"],
            "Limit Value": cfg_dict["Speed"]["Limit Value"]
        },
        "Logging": {
            "Up": cfg_dict["Logging"]["Up"],
            "Down": cfg_dict["Logging"]["Down"]
        }
    }

    test_points = gen.torque_speed_table(gen_cfg)

    if test_dict["Debug"] is True:
        debug.markdown("## Generator Config")
        debug.write(gen_cfg)
        debug.markdown("## Test points")
        debug.write(test_points)

if st.session_state.test_name == "":

    test_name = (
        datetime_format +
        hw_dict["MCU"]["Name"] +
        "_" +
        hw_dict["MCU"]["Sample Letter"] +
        hw_dict["MCU"]["Sample Number"] +
        "_" +
        hw_dict["Motor"]["Manufacturer"] +
        "_" +
        hw_dict["Motor"]["Sample Letter"] +
        hw_dict["Motor"]["Sample Number"] +
        "_" +
        str(int(hw_dict["DCDC"]["Voltage"])) +
        "V_" +
        test_dict["Profile"]
    )

else:
    test_name = (datetime_format + test_dict["Name"])

#test_name
#test_name = "2022_09_15_11_08_45_Derwent_A1_Turntide_A1_300V_Forward"
paths["measured"]= (os.getcwd() + "\\" + test_name + '.csv')
paths["Alert"]= (os.getcwd() + "\\" + test_name + "_ALERTS.csv")
paths["inverter"]= (os.getcwd() + "\\" + test_name + "_INVERTER_PROPERTIES.csv")
paths["current"]= (os.getcwd() + "\\" + test_name + "_CURRENT_SENSOR_LATENCY_TABLE.csv")
paths["45"]= (os.getcwd() + "\\" + test_name + "_45.csv")
paths["0"]= (os.getcwd() + "\\" + test_name + "_0.csv")
paths["Torque Averaged"]= (os.getcwd() + "\\" + test_name + "_TAVG.csv")
paths["Remote"]= (r'N:\UK\Engineering\Documents\Motor Control\Dyno Test Reports' + "\\" + test_dict["Type"])

if test_dict["Type"] == "Torque Speed Sweep":

    speed_demands = test_points.speed_demand.tolist()
    speed_limits_forward = test_points.speed_lim_fwd.tolist()
    speed_limits_reverse = test_points.speed_lim_rev.tolist()
    torque_demands =test_points.torque_demand.tolist()
    demand_periods =test_points.demand_period.tolist()
    logged_points =test_points.test_point.tolist()

    script_input = {
        "Test": {
            "Name":test_name,
            "Export Path":paths["export"],
            "Export Name": constants.INTERNAL_NAME,
            "Export Format": constants.INTERNAL_FORMAT,
            "Type": test_dict["Type"],
            "Profile": test_dict["Profile"],
            "Logging Path": test_dict["Path"],
        },
        "Test Input": {
            "Speed Demands": speed_demands,
            "Speed Limits Forward": speed_limits_forward,
            "Speed Limits Reverse": speed_limits_reverse,
            "Torque Demands": torque_demands,
            "Demand Period": demand_periods,
            "Logged Points": logged_points
        },
        "MCU": hw_dict["MCU"],
        "Motor": hw_dict["Motor"],
        "DCDC": hw_dict["DCDC"],
        "Dyno": hw_dict["Dyno"],
        "CAN": hw_dict["CAN"],
        "Login": hw_dict["Login"],
        "Temp": cfg_dict["Temp"],
        "Speed": cfg_dict["Speed"],
        "Logging": cfg_dict["Logging"]
    }
    
    if test_dict["Debug"] is True:
        debug.markdown("## Script Input")
        debug.write(script_input)
    
    code_path = gen.torque_speed_script(script_input)

    gen.file_merge(
        first_file=code_path,
        second_file=paths["torque speed script"]
    )

    envolope = gen.envelope_table(
            torque_peak=hw_dict["MCU"]["Peak Torque"],
            speed_breakpoints=hw_dict["MCU"]["Speed Breakpoints"],
            voltage_breakpoints=hw_dict["MCU"]["Voltage Breakpoints"],
            voltage_ref=hw_dict["DCDC"]["Voltage"]
    )

    profile_gen_mode = "torque_speed_gen"

elif test_dict["Type"] in ["Idq Injection", "Idq Characterisation"]:

    speed_demands = test_points.speed_demand.tolist()
    speed_limits_forward = test_points.speed_lim_fwd.tolist()
    speed_limits_reverse = test_points.speed_lim_rev.tolist()
    iq_demands =test_points.iq_injected.tolist()
    id_demands =test_points.id_injected.tolist()
    demand_periods =test_points.demand_period.tolist()
    logged_points =test_points.test_point.tolist()

    script_input = {
        "Test": {
            "Name":test_name,
            "Export Path":paths["export"],
            "Export Name": constants.INTERNAL_NAME,
            "Export Format": constants.INTERNAL_FORMAT,
            "Type": test_dict["Type"],
            "Profile": test_dict["Profile"],
            "Logging Path": test_dict["Path"],
        },
        "Test Input": {
            "Speed Demands": speed_demands,
            "Speed Limits Forward": speed_limits_forward,
            "Speed Limits Reverse": speed_limits_reverse,
            "Id Demands": id_demands,
            "Iq Demands": iq_demands,
            "Demand Period": demand_periods,
            "Logged Points": logged_points
        },
        "MCU": hw_dict["MCU"],
        "Motor": hw_dict["Motor"],
        "DCDC": hw_dict["DCDC"],
        "Dyno": hw_dict["Dyno"],
        "CAN": hw_dict["CAN"],
        "Login": hw_dict["Login"],
        "Speed": cfg_dict["Speed"],
        "Idq": cfg_dict["Idq"],
        "Temp": cfg_dict["Temp"],
        "Logging": cfg_dict["Logging"]
    }

    if test_dict["Debug"] is True:
        debug.markdown("## Script Input")
        debug.write(script_input)

    code_path = gen.idq_script(script_input)

    gen.file_merge(
        first_file=code_path,
        second_file=paths["idq script"]
    )

    profile_gen_mode = "idq_inj_gen"

else:
    st.warning("Other Methods in Development")
    st.stop()

plot_dict = ui_plots.tab_plots(r_col, envolope, test_points, hw_dict, profile_gen_mode)
ui_tester.tab_tester(r_col, paths, test_points)

if st.session_state.generate_report is True:
    report_gen.generate(paths, hw_dict, test_dict, cfg_dict, plot_dict, test_points, test_name)

if st.session_state.results_remote is True:
    try:

        moved = report_gen.move_report(
            zip_path=paths["zip"],
            remote_path=paths["Remote"]
        )

        if moved is True:
            st.success(f"Report moved to : {paths['Remote']}/{paths['zip']}")
        else:
            st.error("Report unable to be moved")
            
    except Exception as e:
        st.error(e)
        st.error("Report unable to be moved")