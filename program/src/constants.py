import datapane as dp

AVAILABLE_PROJECTS = ["Derwent", "Bowfell", "Oxford", "Other"]
FIELD_INVERTER_NAME = AVAILABLE_PROJECTS[1]
AVAILABLE_SAMPLE_LETTERS = ["A", "B", "C", "D", "Other"]
FIELD_INVERTER_SAMPLE_LETTER = AVAILABLE_SAMPLE_LETTERS[1]
AVAILABLE_SAMPLE_NUMBERS = ["1", "2", "3", "4"]
FIELD_INVERTER_SAMPLE_NUMBER = AVAILABLE_SAMPLE_NUMBERS[1]
MOTOR_MANUFACTURES = ["Turntide", "Remy", "Yasa", "Integral Powertrain", "Emrax",
                      "Other"
                      ]
SAMPLE_LETTER = ["A", "B", "C", "D", "Other"]
SAMPLE_NUMBER = ["1", "2", "3", "4", "N/A"]
DYNO_LOCATIONS = ["DX: 340kW", "DX: 160kW", "DX: 100kW"]
TEST_TYPES = [
    "Torque Speed Sweep", "Idq Characterisation", "Idq Injection",
    "Udq Injection", "Encoder Alignment", "Back EMF Calculation",
    "Encoder Latency Table", "Current Latency Table"
]
OPERATING_QUADRANTS = [
    "Forward Motoring", "Reverse Generating", "Reverse Motoring",
    "Forward Generating", "Motoring", "Generating", "Forward",
    "Reverse", "All"
]
TORQUE_SENSORS = ['HBM', 'TS', 'None']
INTERNAL_NAME = "test_script"
INTERNAL_FORMAT = ".py"
LV_PSU_VOLTAGE = 16.00
FLT_ZERO = 0.0
LOGIN_TIMER = 10
REPORT_FORMAT = dp.ReportFormatting(
    light_prose=False,
    accent_color="#006AA6",
    bg_color="#FFF",
    text_alignment=dp.TextAlignment.JUSTIFY,
    font=dp.FontChoice.SANS,
    width=dp.ReportWidth.MEDIUM,
    )