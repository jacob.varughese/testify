import streamlit as st
from src import plot


def tab_plots(data_tab, envolope, test_points, hw_dict, test_type):
    '''
    Draw Plotting Tab
    '''

    scatter_tab, line_tab, table_tab = data_tab.tabs(["Scatter", "Line", "Table"])

    scatter_plot = ui_scatter(scatter_tab, envolope, test_points, hw_dict, test_type)
    line_plot = ui_timeline(line_tab, test_points, hw_dict, test_type)
    ui_table(table_tab, test_points)

    plot_dict = {
        "Scatter": scatter_plot,
        "Line": line_plot
    }

    return plot_dict


def ui_scatter(scatter_tab, envolope, test_points, hw_dict, test_type):
    '''
    Draw UI for scatter plot tab
    '''
    with st.spinner("Gererating Scatter Plot"):

        if test_type == "torque_speed_gen":

            test_points.speed_demand_rads = test_points.speed_demand.multiply(2 * 3.14 * (1 / 60))
            test_points.power_mech = abs(test_points.speed_demand_rads.multiply(test_points.torque_demand))

            scatter_plot = plot.torque_speed_scatter(
                profile=test_points,
                profile_voltage=hw_dict["DCDC"]["Voltage"],
                project_speeds=envolope.speed_envelope,
                project_torques=envolope.torque_envelope
            )

        elif test_type == "idq_inj_gen":

            scatter_plot = plot.idq_scatter(
                profile=test_points,
                voltage_ref=hw_dict["DCDC"]["Voltage"]
            )

        scatter_tab.plotly_chart(scatter_plot)

    return scatter_plot


def ui_timeline(line_tab, test_points, hw_dict, test_type):
    '''
    Draw UI for timeline plot tab
    '''
    with st.spinner("Gererating Timeline Plot"):

        if test_type == "torque_speed_gen":
            
            test_points.speed_demand_rads = test_points.speed_demand.multiply(2 * 3.14 * (1 / 60))
            test_points.power_mech = abs(test_points.speed_demand_rads.multiply(test_points.torque_demand))

            line_plot = plot.torque_speed_timeline(
                profile=test_points,
                profile_voltage=hw_dict["DCDC"]["Voltage"],
                speed_limit_forward=test_points.speed_lim_fwd,
                speed_limit_reverse=test_points.speed_lim_rev
            )

        elif test_type == "idq_inj_gen":

            line_plot = plot.idq_timeline(profile=test_points)

        line_tab.plotly_chart(line_plot)

    return line_plot


def ui_table(table_tab, test_points):
    '''
    Draw UI for test input datatable
    '''
    table_tab.markdown("### Test Input Table")
    table_tab.dataframe(test_points)

    return
