import os
import streamlit as st


def tab_profile(profile_tab):
    '''
    Generate the fields for the sidebar
    '''
    TEST_TYPES = [
    "Torque Speed Sweep", "Idq Characterisation", "Idq Injection",
    "Udq Injection", "Encoder Alignment", "Back EMF Calculation",
    "Encoder Latency Table", "Current Latency Table"
    ]

    profile_tab.text_input(
        label="Test Name",
        value="",
        placeholder="AUTO: <DATETIME>_<PROJECT>_<INVERTER>_<MOTOR>_<VOLTAGE>_<PROFILE>",
        key="test_name"
    )

    profile_tab.text_input(
        label="Log Directory",
        value=(os.getcwd() + r"\Log"),
        placeholder=os.getcwd(),
        key="logging_path"
    )

    profile_tab.markdown("---")

    profile_tab.selectbox(
        label="Test Type",
        options=TEST_TYPES,
        key="requested_test"
    )

    if st.session_state.requested_test in ["Torque Speed Sweep","Idq Injection"]:
        OPERATING_QUADRANTS = [
            "Forward Motoring", "Reverse Generating", "Reverse Motoring",
            "Forward Generating", "Motoring", "Generating", "Forward",
            "Reverse", "All"
            ]

    if st.session_state.requested_test in ["Idq Characterisation"]:
        OPERATING_QUADRANTS = ["Forward", "Reverse", "All"]

    profile_tab.selectbox(
        label="Profile",
        options=OPERATING_QUADRANTS,
        key="requested_profile"
    )

    if st.session_state.logging_path == "":
        st.session_state.logging_path = os.getcwd()

    profile_tab.markdown("---")

    profile_tab.checkbox(
        label = "Show Tool Debug",
        value = False,
        key = "tool_debug"
        )

    test_dict = {
        "Path":st.session_state.logging_path,
        "Type":st.session_state.requested_test,
        "Profile":st.session_state.requested_profile,
        "Name":st.session_state.test_name,
        "Debug":st.session_state.tool_debug
    }

    return test_dict