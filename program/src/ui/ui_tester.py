import sys
import streamlit as st
import numpy as np
import sys
from subprocess import Popen, CREATE_NEW_CONSOLE

def convert(seconds):
    '''
    convert secnds to d h s format
    '''
    seconds = seconds % (24 * 3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60

    return (f"{hour}h:{minutes}m:{seconds}s")

def tab_tester(test_tab, paths, test_points):
    '''
    Draw UI for Testing tab
    '''
    test_tab.checkbox(
        label="Generate Report on Completion",
        value=True,
        key="generate_report"
    )

    test_tab.checkbox(
        label=(f"Move to {paths['Remote']}"),
        value=True,
        key="results_remote"
    )

    test_tab.markdown("---")
    st.session_state.Number_of_Points = len(test_points)
    test_tab.markdown(f"Number of Points: `{st.session_state.Number_of_Points}`")
    test_tab.markdown(f"Approximate Test Time: `{convert(np.sum(test_points.demand_period.tolist()))}`")
    test_tab.markdown("---")

    if test_tab.button("Confirm and Start Tests"):
        test_tab.success("Test Setup and Configuration Confirmed, Proceeding with test")

        with st.spinner("Test Script launched and is running in a seperate shell.."):
        # Run the test script in shell so we can see the print statements.
            process = Popen(
                [f"{sys.executable}", paths["script"]],
                creationflags=CREATE_NEW_CONSOLE,
                stdout=sys.stdout
            )
            process.wait()
        test_tab.info("Test Script Complete or Closed")
    else:
        st.stop()