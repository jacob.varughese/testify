import streamlit as st
from src import profiles
from src import constants


def tab_hw(hw_tab):
    '''
    Draw HW tab UI
    '''
    mcu_tab, motor_tab, dcdc_tab, dyno_tab, can_tab, login_tab = hw_tab.tabs(["MCU", "Motor", "DCDC", "Dyno", "CAN", "Login"])
    mcu_dict = ui_mcu(mcu_tab)
    motor_dict = ui_motor(motor_tab, mcu_dict)
    dcdc_dict = ui_dcdc(dcdc_tab, mcu_dict)
    dyno_dict = ui_dyno(dyno_tab)
    can_dict = ui_can(can_tab, dyno_dict)
    login_dict = ui_login(login_tab)

    hw_dict = {
        "MCU": mcu_dict,
        "Motor": motor_dict,
        "DCDC": dcdc_dict,
        "Dyno": dyno_dict,
        "CAN": can_dict,
        "Login": login_dict
    }

    return hw_dict


def ui_mcu(mcu_tab):
    '''
    Draw MCU tab UI
    '''

    # Inverter
    mcu_tab.selectbox(
        label="Project",
        options=constants.AVAILABLE_PROJECTS,
        key="requested_project"
    )

    if st.session_state.requested_project == "Other":
        mcu_tab.text_input(
            label="",
            value="",
            key="requested_project_OTHER"
        )

        field_inverter_name = st.session_state.requested_project_OTHER
    else:
        field_inverter_name = st.session_state.requested_project

    if st.session_state.requested_project == "Bowfell":
        project_profile = profiles.Bowfell_Profile()

    if st.session_state.requested_project == "Oxford":
        project_profile = profiles.Oxford_Profile()

    if st.session_state.requested_project == "Derwent":
        project_profile = profiles.Derwent_Profile()
        
    mcu_tab.selectbox(
        label="Sample Letter",
        options=constants.AVAILABLE_SAMPLE_LETTERS,
        key="inverter_sample_letter"
    )

    if st.session_state.inverter_sample_letter == "Other":
        mcu_tab.text_input(
            label="",
            value="",
            key="inverter_sample_letter_other"
        )

        field_inverter_sample_letter = st.session_state.inverter_sample_letter_other
        inverter_sample_number = "_"
    else:
        field_inverter_sample_letter = st.session_state.inverter_sample_letter
        inverter_sample_number = constants.AVAILABLE_SAMPLE_NUMBERS

    mcu_tab.selectbox(
        label="Sample Number",
        options=inverter_sample_number,
        key="inverter_sample_number"
    )

    if st.session_state.inverter_sample_number != "_":
        field_inverter_sample_number = st.session_state.inverter_sample_number
    else:
        field_inverter_sample_number = "_"

    mcu_tab.text_area(
        label="Inverter Notes",
        placeholder="Hardware modifications, Prototype, Customer return etc.",
        key="inverter_note"
    )

    mcu_tab.number_input(
        label="Discharge Current (+)",
        min_value=0.0,
        max_value=500.0,
        value=400.0,
        step=0.5,
        key="mcu_discharge_i_lim"
    )

    mcu_tab.number_input(
        label="Charge Current (-)",
        min_value=-500.0,
        max_value=0.0,
        value=-400.0,
        step=0.5,
        key="mcu_charge_i_lim"
    )

    mcu_dict = {
        "Name": field_inverter_name,
        "Sample Letter": field_inverter_sample_letter,
        "Sample Number": field_inverter_sample_number,
        "Note": st.session_state.inverter_note,
        "Voltage Breakpoints": project_profile["Voltage Breakpoints"],
        "Speed Breakpoints": project_profile["Speed Breakpoints"],
        "Max Voltage": project_profile["Max Voltage"],
        "Max Speed": project_profile["Max Speed"],
        "Max Torque": project_profile["Max Torque"],
        "Max Stator Current": project_profile["Max Stator Current"],
        "Peak Torque": project_profile["Peak Torque"],
        "Pole Pairs": project_profile["Pole Pairs"],
        "Charge Limit": st.session_state.mcu_charge_i_lim,
        "Discharge Limit": st.session_state.mcu_discharge_i_lim
    }

    return mcu_dict


def ui_motor(motor_tab, mcu_dict):
    '''
    Draw tab UI
    '''
    # Motor
    motor_tab.selectbox(
        label="Manufacturer",
        options=constants.MOTOR_MANUFACTURES,
        key="motor_manufacturer"
    )

    if st.session_state.motor_manufacturer == "Other":
        motor_tab.text_input(
            label="",
            value="",
            key="motor_manufacturer_Other"
        )

    motor_tab.selectbox(
        label="Sample Letter",
        options=constants.SAMPLE_LETTER,
        key="motor_sample_letter"
    )

    if st.session_state.motor_manufacturer == "Other":
        motor_tab.text_input(
            label="",
            value="",
            key="MOTOR_SAMPLE_OTHER"
        )

    motor_tab.selectbox(
        label="Sample Number",
        options=constants.SAMPLE_NUMBER,
        key="motor_sample_number"
    )
    #
    motor_tab.text_area(
        label="Motor Notes",
        placeholder="Hardware modifications, Prototype, Customer return etc.",
        key="motor_note"
    )

    motor_dict = {
        "Manufacturer": st.session_state.motor_manufacturer,
        "Sample Letter": st.session_state.motor_sample_letter,
        "Sample Number": st.session_state.motor_sample_number,
        "Pole Pairs":mcu_dict["Pole Pairs"],
        "Note": st.session_state.motor_note
    }

    return motor_dict


def ui_dcdc(dcdc_tab, mcu_dict):
    '''
    Draw tab UI
    '''
    dcdc_tab.number_input(
        label="DC Link Voltage",
        min_value=float(min(mcu_dict["Voltage Breakpoints"])),
        max_value=float(mcu_dict["Max Voltage"]),
        value=300.0,
        step=0.5,
        help="Project Breakpoints = " + str(mcu_dict["Voltage Breakpoints"]),
        key="requested_voltage"
    )

    dcdc_tab.number_input(
        label="DC Link Current (+)",
        min_value=0.0,
        max_value=500.0,
        value=400.0,
        step=0.5,
        key="requested_i_lim_pos"
    )

    dcdc_tab.number_input(
        label="DC Link Current (-)",
        min_value=-500.0,
        max_value=0.0,
        value=-400.0,
        step=0.5,
        key="requested_i_lim_neg"
    )

    dcdc_dict = {
        "Voltage": st.session_state.requested_voltage,
        "Current +": st.session_state.requested_i_lim_pos,
        "Current -": st.session_state.requested_i_lim_neg
    }

    return dcdc_dict

def ui_dyno(dyno_tab):
    '''
    Draw tab UI
    '''
    dyno_tab.selectbox(
        label="Location",
        options=constants.DYNO_LOCATIONS,
        key="dyno_location"
    )

    if st.session_state.dyno_location == "DX: 340kW":
        DEFAULT_IP = "192.168.1.2"
        DEFAULT_PORT = int(502)
        DEFAULT_ID = int(0)
        DEFAULT_CAN_MCU = "IXXAT HW507598"
        DEFAULT_CAN_DCDC = "IXXAT HW484965"
        DEFAULT_TORQUE_SENSOR = "HBM T40B"
        GEARBOX_RATIO = 3.06

    if st.session_state.dyno_location == "DX: 160kW":
        DEFAULT_IP = "192.168.1.1"
        DEFAULT_PORT = int(502)
        DEFAULT_ID = int(0)
        DEFAULT_CAN_MCU = "IXXAT HW123456"
        DEFAULT_CAN_DCDC = "IXXAT HW79010"
        DEFAULT_TORQUE_SENSOR = "HBM T40B"
        GEARBOX_RATIO = 3.06

    if st.session_state.dyno_location == "DX: 100kW":
        DEFAULT_IP = "192.168.1.100"
        DEFAULT_PORT = int(502)
        DEFAULT_ID = int(0)
        DEFAULT_CAN_MCU = "IXXAT HW429680"
        DEFAULT_CAN_DCDC = "IXXAT HW500814"
        DEFAULT_TORQUE_SENSOR = "TS"
        GEARBOX_RATIO = 3.00

    dyno_tab.text_input(
        label="IP Address",
        value=DEFAULT_IP,
        key="dyno_ip"
    )

    dyno_tab.number_input(
        label="Port",
        min_value=0,
        max_value=5000,
        value=DEFAULT_PORT,
        step=1,
        key="dyno_port"
    )

    dyno_tab.number_input(
        label="ID",
        min_value=0,
        max_value=64,
        value=DEFAULT_ID,
        step=1,
        key="dyno_id"
    )

    # dyno_tab.selectbox(
    #     label="Torque Transducer",
    #     options=[DEFAULT_TORQUE_SENSOR],
    #     key="torque_sensor_type",
    #     help=str(["TS","HBM","None"])
    # )


    dyno_tab.number_input(
        label="Torque Sensor Filter",
        min_value=0,
        max_value=512,
        value=128,
        step=1,
        key="torque_sensor_torque_filter"
        ) 

    dyno_dict = {
        "Location": st.session_state.dyno_location,
        "IP": st.session_state.dyno_ip,
        "ID": st.session_state.dyno_id,
        "Port": st.session_state.dyno_port,
        "Gearbox Ratio": GEARBOX_RATIO,
        "Transducer": DEFAULT_TORQUE_SENSOR,
        "Transducer Torque Filter": st.session_state.torque_sensor_torque_filter,
        "MCU CAN ID": DEFAULT_CAN_MCU,
        "DCDC CAN ID": DEFAULT_CAN_DCDC
    }

    return dyno_dict

def ui_can(can_tab, dyno_dict):
    '''
    Draw tab UI
    '''
    can_tab.text_input(
        label="DCDC CAN HWID",
        value=dyno_dict["DCDC CAN ID"],
        key="can_hwid_dcdc"
    )

    can_tab.text_input(
        label="MCU CAN HWID",
        value=dyno_dict["MCU CAN ID"],
        key="can_hwid_mcu"
    )

    can_dict = {
        "MCU CAN HWID": st.session_state.can_hwid_mcu,
        "DCDC CAN HWID": st.session_state.can_hwid_dcdc
    }

    return can_dict

def ui_login(login_tab):
    '''
    Draw tab UI
    '''
    login_tab.checkbox(
        label="Login",
        value=True,
        key="login_user"
    )

    login_tab.text_input(
        label="Password",
        value="00000000000000000000000000000000",
        placeholder="00000000000000000000000000000000",
        key="mcu_password"
    )

    login_tab.markdown(f"`{len(st.session_state.mcu_password)}`")

    login_dict = {
        "Login":st.session_state.login_user,
        "Password":st.session_state.mcu_password
    }

    return login_dict
