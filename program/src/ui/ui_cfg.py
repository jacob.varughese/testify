from cProfile import label
import os
import profile
from turtle import speed
import streamlit as st
from src import profiles
from src import constants

def tab_cfg(cfg_tab, hw_dict, test_dict):
    '''
    Draw CFG tab UI
    '''
    if test_dict["Type"] in ["Torque Speed Sweep"]:
        logging_tab, speed_tab, torque_tab, temp_tab = cfg_tab.tabs(["Logging", "Speed", "Torque", "Temperature"])
        
        logging_dict = ui_logging(logging_tab)
        speed_dict = ui_speed(speed_tab, hw_dict)
        torque_dict = ui_torque(torque_tab, hw_dict)
        temp_dict = ui_temp(temp_tab)

        cfg_dict = {
            "Logging":logging_dict,
            "Speed":speed_dict,
            "Torque":torque_dict,
            "Temp":temp_dict
        }

    if test_dict["Type"] in ["Idq Injection", "Idq Characterisation"]:
        logging_tab, speed_tab, idq_tab, temp_tab = cfg_tab.tabs(["Logging", "Speed", "Idq", "Temperature"])

        logging_dict = ui_logging(logging_tab) 
        speed_dict = ui_speed(speed_tab, hw_dict)
        idq_dict = ui_idq(idq_tab, hw_dict, logging_dict)
        temp_dict = ui_temp(temp_tab)

        cfg_dict = {
            "Logging": logging_dict,
            "Speed": speed_dict,
            "Idq": idq_dict,
            "Temp": temp_dict
        }

    return cfg_dict


def ui_logging(logging_tab):
    '''
    Draw Logging Tab UI
    '''
    logging_tab.checkbox(
        label="Log On Increasing Step",
        value=True,
        key="log_up",
        help="Enable Logging once steady state reached during the ramp up test points"
    )

    logging_tab.checkbox(
        label="Log On Decreasing Step",
        value=False,
        key="log_down",
        help="Enable Logging once steady state reached during the ramp down test points"
    )
    
    logging_tab.number_input(
        label="Logging Sample Period [s]",
        min_value=0.01,
        max_value=10.0,
        value=0.1,
        step=0.1,
        key="log_period"
    )

    logging_tab.checkbox(
        label="Delay Logging",
        value=True,
        key="delay_log",
        help="Delay data capture to allow steady state condition before capturing data."
    )

    logging_tab.number_input(
        label="Delay Period [s]",
        min_value=0.01,
        max_value=10.0,
        value=0.2,
        step=0.01,
        key="delay_period",
        disabled= not st.session_state.delay_log
    )

    logging_dict = {
        "Up":st.session_state.log_up,
        "Down":st.session_state.log_down,
        "Sample Period":st.session_state.log_period,
        "Delay":st.session_state.delay_log,
        "Delay Period":st.session_state.delay_period
    }

    return logging_dict


def ui_speed(speed_tab, hw_dict):
    '''
    Draw Speed config Tab UI
    '''
    speed_tab.selectbox(
        label="Speed Limit Threshold",
        options=['Percentage', 'Offset', 'Value'],
        key="requested_speed_limit_type",
        disabled=True
    )

    if st.session_state.requested_speed_limit_type == "Percentage":
        speed_tab.number_input(
            label="Percentage of Speed Target ",
            min_value=0,
            max_value=1000,
            value=120,
            step=1,
            key="requested_speed_limit_value"
        )

    elif st.session_state.requested_speed_limit_type == "Offset":
        speed_tab.number_input(
            label="Offset",
            min_value=0.0,
            max_value=float(hw_dict["MCU"]["Max Speed"]),
            value=500.0,
            step=0.01,
            key="requested_speed_limit_value"
        )

    elif st.session_state.requested_speed_limit_type == "Value":
        speed_tab.number_input(
            label="Offset",
            min_value=0.0,
            max_value=float(hw_dict["MCU"]["Max Speed"]),
            value=float(hw_dict["MCU"]["Max Speed"]),
            step=0.01,
            key="requested_speed_limit_value"
        )

    if st.session_state.requested_test == "Torque Speed Sweep":
        speed_tab.number_input(
            label="Minimum Speed",
            min_value=0.0,
            max_value=float(hw_dict["MCU"]["Max Speed"]),
            value=500.0,
            step=0.5,
            key="requested_min_speed"
        )

        speed_tab.number_input(
            label="Maximum Speed",
            min_value=0.5,
            max_value=float(hw_dict["MCU"]["Max Speed"]),
            value=float(hw_dict["MCU"]["Max Speed"]),
            step=0.5,
            key="requested_max_speed"
        )

        speed_tab.number_input(
            label="Speed Step Size",
            min_value=0.5,
            max_value=float(hw_dict["MCU"]["Max Speed"]),
            value=500.0,
            step=0.5,
            key="requested_speed_step"
        )

        speed_dict = {
            "Limit Type":st.session_state.requested_speed_limit_type,
            "Limit Value":st.session_state.requested_speed_limit_value,
            "Minimum":st.session_state.requested_min_speed,
            "Maximum":st.session_state.requested_max_speed,
            "Step":st.session_state.requested_speed_step
        }

    if st.session_state.requested_test in ["Idq Injection", "Idq Characterisation"]:

        speed_tab.number_input(
            label="Test Speed",
            min_value=0.0,
            max_value=float(hw_dict["MCU"]["Max Speed"]),
            value=500.0,
            step=0.5,
            key="requested_speed_idq"
        )

        speed_dict = {
            "Limit Type":st.session_state.requested_speed_limit_type,
            "Limit Value":st.session_state.requested_speed_limit_value,
            "Target":st.session_state.requested_speed_idq
        }

    return speed_dict


def ui_torque(torque_tab, hw_dict):
    '''
    Draw UI for torque Tab
    '''
    torque_tab.number_input(
        label="Minimum Torque (abs[Nm])",
        min_value=0.0,
        max_value=float(hw_dict["MCU"]["Max Torque"]),
        value=0.0,
        step=0.5,
        key="requested_min_torque"
    )

    torque_tab.number_input(
        label="Maximum Torque (% of Peak)",
        min_value=0,
        max_value=100,
        value=100,
        step=1,
        key="requested_max_torque"
    )

    torque_tab.number_input(
        label="Step Up Size [Nm]",
        min_value=0.5,
        max_value=float(hw_dict["MCU"]["Max Torque"]),
        value=20.0,
        step=0.5,
        key="requested_torque_step_up"
    )

    torque_tab.number_input(
        label="Step Up Period [s]",
        min_value=0.01,
        max_value=5000.0,
        value=1.0,
        step=0.01,
        key="requested_torque_step_up_period"
    )

    torque_tab.number_input(
        label="Step Down Size [Nm]",
        min_value=0.01,
        max_value=float(hw_dict["MCU"]["Max Torque"]),
        value=20.0,
        step=0.5,
        key="requested_torque_step_down"
    )

    torque_tab.number_input(
        label="Step Down Period [s]",
        min_value=0.01,
        max_value=5000.0,
        value=1.0,
        step=0.01,
        key="requested_torque_step_down_period"
    )

    torque_tab.checkbox(
        label="Skip last Torque demand per speed step",
        value=True,
        key="skip_max_torque"
    )

    torque_dict = {
        "Minimum":st.session_state.requested_min_torque,
        "Maximum":st.session_state.requested_max_torque,
        "Ramp Up Torque":st.session_state.requested_torque_step_up,
        "Ramp Down Torque":st.session_state.requested_torque_step_down,
        "Ramp Up Period":st.session_state.requested_torque_step_up_period,
        "Ramp Down Period":st.session_state.requested_torque_step_down_period,
        "Skip":st.session_state.skip_max_torque
    }

    return torque_dict

def ui_temp(temp_tab):
    '''
    Draw UI for temperature tab
    '''
    if temp_tab.checkbox(
        label="Enable Temperature Dwelling",
        value=True,
        key="motor_dwell_enable"
    ):

        temp_tab.number_input(
            label="Upper Motor [C]",
            min_value=0.0,
            value=80.0,
            max_value=120.0,
            step=1.0,
            key="motor_temp_upper",
            help="Maximum Motor Temperature allowed during current injection."
        )

        temp_tab.number_input(
            label="Lower Motor [C]",
            min_value=0.0,
            value=60.0,
            max_value=120.0,
            step=1.0,
            key="motor_temp_lower",
            help="Motor Temperature starting allowed during current injection."
        )

        temp_tab.number_input(
            label="Ramp Step",
            min_value=0.0,
            value=10.0,
            max_value=100.0,
            step=1.0,
            key="motor_temp_step",
            help="Current or Torque ramp up and down step when temp dwell is running."
        )

        temp_dict = {
            "Enable":st.session_state.motor_dwell_enable,
            "Limit Upper":st.session_state.motor_temp_upper,
            "Limit Lower":st.session_state.motor_temp_lower,
            "Step":st.session_state.motor_temp_step
        }
    
    else:
        temp_dict = {
            "Enable":st.session_state.motor_dwell_enable
        }
    
    return temp_dict


def ui_idq(idq_tab, hw_dict, logging_dict):
    '''
    Draw UI for IDQ tab
    '''
    idq_tab.number_input(
        label="Minimum Current",
        min_value=0.0,
        max_value=float(hw_dict["MCU"]["Max Stator Current"]),
        value=0.0,
        step=0.5,
        key="requested_current_min"
    )

    idq_tab.number_input(
        label="Maximum Current (Stator)",
        min_value=0.0,
        max_value=float(hw_dict["MCU"]["Max Stator Current"]),
        value=float(hw_dict["MCU"]["Max Stator Current"]),
        step=1.0,
        key="requested_current_max"
    )

    idq_tab.number_input(
        label="Idq Injection Step Size [A]",
        min_value=0.1,
        max_value=float(hw_dict["MCU"]["Max Stator Current"]),
        value=25.0,
        step=0.1,
        key="requested_i_injection_step"
    )

    idq_tab.number_input(
        label="Step Period [s]",
        min_value=0.01,
        max_value=5000.0,
        value=1.0,
        step=0.01,
        key="requested_i_injection_period"
    )

    idq_tab.selectbox(
        label="Step Method",
        options=["Zero","Ramp"],
        key="idq_step_method"
    )

    if st.session_state.idq_step_method == "Ramp":

        idq_tab.number_input(
            label="Ramp Period [s]",
            min_value=0.01,
            max_value=5000.0,
            value=1.0,
            step=0.01,
            key="idq_ramp_period",
            help="Period for ramping up and down to target Idq"
        )

    idq_tab.text_input(
        label="Test Mode ID :",
        value="0x40570027",
        key="test_mode_id"
    )

    idq_tab.text_input(
        label="Idq Axis ID :",
        value="0x40570028",
        key="tmode_idq_axis_id"
    )

    idq_tab.number_input(
        label="Test Axis Value :",
        min_value=0.0,
        value=0.0,
        max_value=10.0,
        step=1.0,
        key="tmode_idq_axis_value"
    )

    idq_tab.text_input(
        label="Id Current Injection ID :",
        value="0x40570029",
        key="tmode_idq_id_id"
    )

    idq_tab.text_input(
        label="Iq Current Injection ID :",
        value="0x4057002A",
        key="tmode_idq_iq_id"
    )

    idq_dict = {
        "Maximum": st.session_state.requested_current_max,
        "Minimum": st.session_state.requested_current_min,
        "Step": st.session_state.requested_i_injection_step,
        "Step Period": st.session_state.requested_i_injection_period,
        "Step Method": st.session_state.idq_step_method,
        "Ramp Period": None,
        "Mode ID": st.session_state.test_mode_id,
        "Axis ID": st.session_state.tmode_idq_axis_id,
        "Axis Value": st.session_state.tmode_idq_axis_value,
        "Id ID": st.session_state.tmode_idq_id_id,
        "Iq ID": st.session_state.tmode_idq_iq_id,
    }

    if idq_dict["Step Method"] == "Ramp":
        idq_dict["Step Period"] = st.session_state.idq_ramp_period

    return idq_dict
