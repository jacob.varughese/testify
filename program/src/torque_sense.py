import ctypes
import os
from enum import Enum
# https://www.sensors.co.uk/files/dsm/Manual_DLL_Guide.pdf
# https://www.sensors.co.uk/sw_downloads/products/torque/v5/STDLL_5-6-2_20200303.zip
class Status(Enum):
    '''
    Sensor Technologies Respose Codes
    '''
    SUCCESS = 0
    BUSY = 1
    COMMAND_ACTIVE = 2
    COMMAND_PENDING = 3
    COMMAND_COMPLETE = 4
    COMMAND_INACTIVE = 5
    FAILURE = 6
    DEVICE_NOT_OPEN = 7
    CHECKSUM_ERROR = 8
    DEVICE_INVALID = 9
    BUFFER_TOO_SMALL = 10
    NOT_AVAILABLE_IN_FIRMWARE = 11
    NO_COMMUNICATIONS_IN_PROGRESS = 12
    SEARCH_IN_PROGRESS = 13
    TOO_MANY_REQUESTS = 14
    ACCESS_VIOLATIONS = 15
    FEATURE_NOT_FITTED = 16
    PARAMETER_ERROR = 17
    SPEED_NOT_FITTED = 19
    ANALOG_SELECTION_INVALID = 20
    ANALOG_CHANNEL_NOT_CALIBRATED = 21
    INTERNAL_FLAG = 22
    INTERNAL_BUFFER_OVERFLOW = 23
    ONE_MILLISECOND_TIMER_RESOLUTION_NOT_POSSIBLE = 24


# Load Linked library
torqueSenseLib = ctypes.CDLL(os.getcwd() + "\\program\\dependencies\\torque_sensor\\stcommdll_v5u_x64.dll")

# DECLARE CTYPE FUCNTIONS, INPUT and return TYPES
'''
<pythonFuctionName>            = <library>.<C_Function>
<pythonFuctionName>.argtype(s) = [<C_Type_Input(s)>]
<pythonFuctionName>.restype    = <C_Type_Return>
'''

transducerFind = torqueSenseLib.ST_Find_Devices_Ex
transducerFind.argtypes = [ctypes.POINTER(ctypes.c_ulong), ctypes.c_ulong, ctypes.c_char, ctypes.c_bool]
transducerFind.restype = ctypes.c_uint
'''
Parameters:
    [devices]: point to number of devices found.
    [filter] : 0 All, 1 eth, 2, RS232, 4 USB 8custom
    [wait]   : wait until discovery function has completed.
'''


transducerOpen = torqueSenseLib.ST_Open_Device
transducerOpen.argtypes = [ctypes.c_ulong]
transducerOpen.restype = ctypes.c_uint
'''
Parameters:
    [device_id]: device id of the transducer to open, ids are indexed from 0 up to the number of transducers found.
'''

transducerClose = torqueSenseLib.ST_Close_Device
transducerClose.argtypes = [ctypes.c_ulong, ctypes.c_bool]
transducerClose.restype = ctypes.c_uint
'''
Parameters:
    [device_id]: device id of the transducer to close, ids are indexed from 0 up to the number of transducers found.
    [force]    : reserved  - set to false
'''

torqueZero = torqueSenseLib.ST_Zero_Transducer
torqueZero.argtypes = [ctypes.c_ulong]
torqueZero.restype = ctypes.c_uint
'''
Parameters:
    [device_id]: device id of the transducer to zero, ids are indexed from 0 up to the number of transducers found.
'''

torqueZeroAverage = torqueSenseLib.ST_ZeroAverage_Transducer
torqueZeroAverage.argtypes = [ctypes.c_ulong]
torqueZeroAverage.restype = ctypes.c_uint
'''
Parameters:
    [device_id]: device id of the transducer to zero over 32 samples, ids are indexed from 0 up to the number of transducers found.
'''

torqueGet = torqueSenseLib.ST_GET_Torque
torqueGet.argtypes = [ctypes.c_ulong, ctypes.POINTER(ctypes.c_float)]
torqueGet.restype = ctypes.c_uint
'''
Parameters:
    [device_id] : device id of the transducer to get torque value, ids are indexed from 0 up to the number of transducers found.
    [torque_val]: pointer to torque measured.
'''

torqueGetFilter = torqueSenseLib.ST_GET_Torque_Filter
torqueGetFilter.argtypes = [ctypes.c_ulong, ctypes.POINTER(ctypes.c_ulong)]
torqueGetFilter.restype = ctypes.c_uint
'''
Parameters:
    [device_id] : device id of the transducer to open, ids are indexed from 0 up to the number of transducers found.
    [filter_val]: pointer to put the transducer filter val
'''

torqueSetFilter = torqueSenseLib.ST_SET_Torque_Filter
torqueSetFilter.argtypes = [ctypes.c_ulong, ctypes.c_ulong, ctypes.c_bool]
torqueSetFilter.restype = ctypes.c_uint
'''
Parameters:
    [device_id] : device id of the transducer to open, ids are indexed from 0 up to the number of transducers found.
    [filter_val]: value to set the filter too
    [save]      : save value in the device on power cycle
'''

speedGetFast = torqueSenseLib.ST_GET_Speed_Fast
speedGetFast.argtypes = [ctypes.c_ulong, ctypes.POINTER(ctypes.c_ulong)]
speedGetFast.restype = ctypes.c_uint
'''
Parameters:
    [device_id]: device id of the transducer to open, ids are indexed from 0 up to the number of transducers found.
    [speed_val]: pointer to store speed value in fast mode.
'''

speedGetSlow = torqueSenseLib.ST_GET_Speed_Slow
speedGetSlow.argtypes = [ctypes.c_ulong, ctypes.POINTER(ctypes.c_ulong)]
speedGetSlow.restype = ctypes.c_uint
'''
Parameters:
    [device_id]: device id of the transducer to open, ids are indexed from 0 up to the number of transducers found.
    [speed_val]: pointer to store speed vlaue in slow mode.
'''

speedGetFilter = torqueSenseLib.ST_GET_Speed_Filter
speedGetFilter.argtypes = [ctypes.c_ulong, ctypes.POINTER(ctypes.c_ulong)]
speedGetFilter.restype = ctypes.c_uint
'''
Parameters:
    [device_id] : device id of the transducer to open, ids are indexed from 0 up to the number of transducers found.
    [filter_val]: pointer to save filter value of the speed filter
'''

speedSetFilter = torqueSenseLib.ST_SET_Speed_Filter
speedSetFilter.argtypes = [ctypes.c_ulong, ctypes.c_ulong, ctypes.c_bool]
speedSetFilter.restype = ctypes.c_uint
'''
Parameters:
    [device_id] : device id of the transducer to open, ids are indexed from 0 up to the number of transducers found.
    [filter_val]: filter value of the speed filter to set
    [save]      : save new value in device after power cycle
'''
# TODO: ADD more functions, i.e. get/set cap rate, shaft temp, calib date

# Constants
devices_found = ctypes.c_ulong(0)
first_device = ctypes.c_ulong(0)
Filter_USB = ctypes.c_ulong(4)
TRUE = ctypes.c_bool(True)
FALSE = ctypes.c_bool(False)
measuredTorque = ctypes.c_float(0.0)
measuredTorqueFilter = ctypes.c_ulong(0)
measuredSpeed = ctypes.c_ulong(0)
measuredSpeedFilter = ctypes.c_ulong(0)

# # Find transducer connected.
# find_device_status = transducerFind(ctypes.byref(devices_found), Filter_USB, 0,TRUE)
# print(find_device_status)
# while find_device_status == Status.SearchInProgress.value:
#     print(find_device_status)

# # If a device is found and the return is successful.
# if (find_device_status == Status.Success.value) and (devices_found.value > 0) :
#     print("Found Device ? [0] = TRUE : {}".format(find_device_status))
#     print("Number of Devices         : {}".format(devices_found.value))

#     transducerOpen(first_device)
#     torqueZeroAverage(first_device)

#     speedGetFilter(first_device, measuredSpeedFilter)
#     print("Speed Filter Val          : {}".format(measuredSpeedFilter.value))
#     torqueGetFilter(first_device, measuredTorqueFilter)
#     print("Torque Filter Val         : {}".format(measuredTorqueFilter.value))
#     speedGetFast(first_device, measuredSpeed)
#     print("Speed                     : {0:.3f} rpm".format(measuredSpeed.value))
#     torqueGet(first_device, measuredTorque)
#     print("Torque                    : {0:.3f} Nm".format(measuredTorque.value))

#     transducerClose(first_device, FALSE)

# else:
#     print("Devices not found")
