# Idq Script
The idq script is used to inject current into the eMotor within the stationary reference frame.
**Note:** Code snippets are simplified versions of the code implemented, that being they omit any debug functions for clarity.

## MunchPy
Post MunchPy is setup and interfaced with the test environment.

### MunchPy Interop
MunchPy launches CANDI and sets the logging path for itself, where information on events and errors can be found.

```python
munch_interop = mpy.MunchInterop.getInstance()

if (munch_interop is not None) and (test_fail is False):

    munch_interop.initialise(

        loggingPath=LOGGING_PATH,

        keepCandiAlive=True

    )

    try:

        if munch_interop.getClient().connect():
```

### Attaching CAN Hardware
If a connection can be made between Munch and CANDI, external CAN hardware  can be setup.

`MCUCanDevice` is attached to the HWID in the string `CAN_HWID_MCU`, i.e. `IXXAT HW203841`. This will search the USB ports for device with that identifier. 
Similarly for `DCDCCanDevice`.

```python
if munch_interop.getClient().connect():

	MCUCanDevice = mpy.CANDevice.getSpecific(

		munchInterop=munch_interop,

		name=CAN_HWID_MCU

	)

	DCDCCanDevice = mpy.CANDevice.getSpecific(

		munchInterop=munch_interop,

		name=CAN_HWID_DCDC

	)
```

Due to there being multiple topologies across the test lab, some logic is applied to setup the correct configuration. 
For example, installed on the 100kW dyno, is a single DCDC converter, whereas the 345kW has two, in parallel, therefore the CAN messaging between the two is different.

To keep the script readable the following is used throughout the script:
- `dcdc` is used to assign the class of the DCDC configuration
- `mcu` is used to assign the class of the motor control unit.
- `dyno` is used to assign the class of the load motor drive.

```python
if DYNO_LOCATION == 'DX: 100kW':

	dcdc = mpy.SingleDCDC(

		munchInterop=munch_interop,

		canDevice=DCDCCanDevice

	)

else:

	dcdc = mpy.ParallelDCDC(

		munchInterop=munch_interop,

		canDevice=DCDCCanDevice

	)

mcu = mpy.Derwent(

	munchInterop=munch_interop,

	canDevice=MCUCanDevice

)

dyno = mpy.NidecDyno(

	munchInterop=munch_interop,

	ipAddress=DYNO_IP,

	ipPort=DYNO_PORT,

	unitId=DYNO_ID

)
```

### Pre-test workflow
Before the test can begin, the following sequence needs to be followed:
`ENABLE LV PSU -> LOGIN TO MCU -> CONFIGURE TESTMODE PROPERTIES -> SET MCU LIMITS -> SETUP DCDC -> ENABLE DCDC -> ENABLE DYNO` 

#### Low Voltage
Using the script `program\src\tenma.py` we can turn the MCU LV power supply on and off. 
**Note:** the 2 second pause `time.sleep(2)`. This is due to internal capacitors keeping the processors awake, if any faults are present, they may still reside in memory, causing the MCU to be in a safe state, therefore the bridge cannot be enabled, until a full key cycle.

Where:
- `FLT_ZERO = 0.0`
- `LV_PSU_VOLTAGE = 16.00`

```python
lv_psu = tenma.tenma_get_device()

tenma.tenma_voltage_target(lv_psu, FLT_ZERO)

time.sleep(2)

tenma.tenma_voltage_target(lv_psu, LV_PSU_VOLTAGE)

tenma.tenma_enable_output(lv_psu)
```

#### MCU Login
Currently on the GEN5 platform there is a timer to prevent logging in on startup. Once this expires we can use the `baseCommsLogin` function to send the password to the MCU (16 characters in length, as a string)

```python
while LOGIN_TIMER > 0:

	time.sleep(1)

	LOGIN_TIMER = LOGIN_TIMER - 1

try:

	mcu.baseCommsLogin(password=PASSWORD)
```

#### Test Mode Configuration
Prior to the bridge enabling, the test mode properties need configured.
This is done using `writeBaseCommsProperty`.

Where, for example:
- `TEST_MODE_ID = "0x40570027"`
- `TEST_MODE_VAL = 1.0`
**Note**: the ID value is a string of a hex value.

```python
mcu.writeBaseCommsProperty(
   propertyId = TEST_MODE_ID, 
   value = TMODE_MODE_VAL, 
   propertyType="float"
   )
						   
 mcu.writeBaseCommsProperty(
	 propertyId = TMODE_AXIS_ID, 
	 value = TMODE_AXIS_VAL, 
	 propertyType="float"
 )
```

#### MCU Limits
Set the MCU current limits using `setCurrentLimits()` passing in a discharge and charge current limit, for the DC Link.

```python
mcu.setCurrentLimits(

	dischargeCurrent=MCU_DISCHARGE_I_LIMIT,

	rechargeCurrent=MCU_CHARGE_I_LIMIT

)
```

#### DCDC Enable
Similarly to MCU Limits, the DCDC Limits accept a value for positive current and negative current for the DC Link.

Once, the current limits have been set, the DCDC can be enabled and a Voltage target set.

```python
dcdc.setOutputCurrentLimits(

	positiveCurrent=DCDC_I_LIMIT_POS,

	negativeCurrent=DCDC_I_LIMIT_NEG

)

dcdc.setOutputEnabled(True)

dcdc.setOutputVoltage(DCDC_V_TARGET)
```

#### Dyno Enable
The Control Techniques cabinets contain separate inverters and drive units, they both need to be enabled separately before a speed demand can be given.

```python
dyno.setDriveEnabled(True)

dyno.setInverterEnabled(True)
```

### Test
Once the test environment has been configured and enabled, the test procedure can begin.

The procedure being:
`ENABLE BRIDGE -> SET SPEED POINT -> INJECT CURRENT -> MEASURE AND AVERAGE -> STOP AND SAVE`

#### Enable Bridge
Send the enable bridge signal to allow voltage to be applied to the MCU / motor terminals.

```python
mcu.setBridgeEnabled(True)
```

#### Check Bridge State
For safety reasons, a check on the bridge state is done every test point, if the bridge state is not `TORQUE_ENABLED[0]`, Idq currents , DCDC and dyno speed are set to 0.

```python
if mcu.getBridgeState() != 0:

	mcu.writeBaseCommsProperty(
		propertyId = IQ_INJ_ID, 
		value = FLT_ZERO, 
		propertyType="float")

	mcu.writeBaseCommsProperty(
		propertyId = ID_INJ_ID, 
		value = FLT_ZERO, 
		propertyType="float")

	dcdc.setOutputVoltage(FLT_ZERO)

	dyno.setPresetReference1(FLT_ZERO)

	test_fail = True

	break
```

#### Set Speed Point
The maximum speed limit is set to 120% of that of the maximum speed demand.
Due to some of the dynameters having gearboxes, a ratio is defined and used to set the correct speed in relation to the eMotor.

As the test generator produces demands for every test point, a for loop is used to step to the next speed point for every test point, despite if the value is the same.

```python
dyno.setMaximumReferenceClamp(
max(SPEED_DEMANDS) * 1.2 / GEARBOX_RATIO
)

for step in range(len(SPEED_DEMANDS)):

	dyno.setPresetReference1(SPEED_DEMANDS[step] / GEARBOX_RATIO)
```

Every speed step a transducer zeroing is also completed to null the torque sensor due to drag losses.

```python
average_torque_transducer()
```

#### Current injection
The script will try three times to inject the Idq demands, if this does not succeed it will not try any more.

```python
for attempt in range(3):

	try:
								                                    
		mcu.writeBaseCommsProperty(
			propertyId = IQ_INJ_ID, 
			value = float(IQ_DEMANDS[step]), 
			propertyType= "float")
		
		mcu.writeBaseCommsProperty(
			propertyId = ID_INJ_ID, 
			value = float(ID_DEMANDS[step]), 
			propertyType= "float")
		
		break
```

#### Measurement

##### Delay Logging
Before logging, an option is given to delay the measurement, in order to avoid the tail end of the transient

```python
if DELAY_LOGGING is True:

	time.sleep(LOG_DELAY_PERIOD)
```

##### Get Data
The measurement is done through several functions, one per signal.
They are added up over the measurement period (`LOOP_SAMPLE_TIME`).

**NOTE:** AlertID and BlockID are not part of the measurement, but they are logged in their own seperate file at the measurement period, this to catch and log any faults that may arrise during the test point.

```python
dclink_voltage = dclink_voltage + mcu.getVdc()

dclink_current = dclink_current + mcu.getIdc()

ts.torqueGet(FIRST_DEVICE, measuredTorque)

torque_measured = torque_measured + (measuredTorque.value / GEARBOX_RATIO)

ts.speedGetFast(FIRST_DEVICE, measuredSpeed)

speed_measured_rpm = speed_measured_rpm + (measuredSpeed.value * GEARBOX_RATIO)

speed_emotor_rpm = speed_emotor_rpm + mcu.getSpeed()

id_feedback = id_feedback + mcu.getIdCurrentFeedback()

iq_feedback = iq_feedback + mcu.getIqCurrentFeedback()

ud_feedback = ud_feedback + mcu.getUdVoltageFeedback()

uq_feedback = ud_feedback + mcu.getUqVoltageFeedback()

motor_temperature = motor_temperature + mcu.getMotorTemperature()

inverter_temperature = inverter_temperature + mcu.getInverterTemperature()

array_alert_id.append(hex(int(mcu.getAlertID())))

array_block_id.append(hex(int(mcu.getBlockID())))

loop_count = loop_count + 1

time.sleep(LOOP_SAMPLE_TIME)
```

Once measurement has been taken, the sum values are divided by the samples to get the mean of each signal.

##### Calculated Values
Using the data captured above, additional values can be calculated for analysis.

`speed_emotor_rads'
$$
eMotorSpeed_{Rad/s} = eMotorSpeed_{rpm} \frac{2\pi}{60}
$$
`speed_measured_rads`
$$
MeasuredSpeed_{Rad/s} = MeasuredSpeed_{rpm} \frac{2\pi}{60}
$$
`mechainical_power`
$$
MechainicalPower = \tau\omega
$$
`is_injected`
$$
Is_{Injected} = \sqrt{iq_{injected}^2+id_{injected}^2}
$$
`is_injected_angle`
$$
Is_\theta = \arctan{\frac{abs(id_{Injected})}{abs(iq_{Injected})}}
$$
`is_injected_angle_degrees` 
$$
Is_\omega = Is_\theta \frac{180}{\pi}
$$

`is_feedback`
$$
Is_{feedback} = \sqrt{iq_{feedback}^2+id_{feedback}^2}
$$
`is_feedback_angle`
$$
Is_\theta = \arctan{\frac{abs(id_{feedback})}{abs(iq_{feedback})}}
$$
`is_feedback_angle_degrees` 
$$
Is_\omega = Is_\theta \frac{180}{\pi}
$$
```python
log_point_current = LOG_POINT[step]

speed_demanded = SPEED_DEMANDS[step]

speed_limit_forward = SPEED_LIMITS_FORWARD[step]

speed_limit_reverse = SPEED_LIMITS_REVERSE[step]

speed_emotor_rads = speed_emotor_rpm * np.radians(6)

speed_measured_rads = speed_measured_rpm * np.radians(6)

mechanical_power = torque_measured * speed_measured_rads

id_injected = ID_DEMANDS[step]

iq_injected = IQ_DEMANDS[step]

is_injected = np.sqrt((id_injected * id_injected) + (iq_injected * iq_injected))

is_injected_angle_radians = np.arctan2(np.abs(id_injected), np.abs(iq_injected))

is_injected_angle_degrees = np.rad2deg(is_injected_angle_radians)

is_feedback = np.sqrt((id_feedback * id_feedback) + (iq_feedback * iq_feedback))

is_feedback_angle_radians = np.arctan2(np.abs(id_feedback), np.abs(iq_feedback))

is_feedback_angle_degrees = np.rad2deg(is_feedback_angle_radians)
```

#### Stop And Save
##### Stop
Once the maximum length of the test point arrays has been reached, the script will then set all equipment values to zero, wait for them to reach zero and disable them.

```python

    mcu.writeBaseCommsProperty(IQ_INJ_ID, FLT_ZERO, propertyType="float")

    mcu.writeBaseCommsProperty(ID_INJ_ID, FLT_ZERO, propertyType="float")

if abs(dyno.getSpeedFeedback()) > 5.0:

        dyno.setPresetReference1(FLT_ZERO)

        while abs(dyno.getSpeedFeedback()) > 5.0:

            time.sleep(1.0)

dcdc.setOutputVoltage(FLT_ZERO)

if DYNO_LOCATION == 'DX: 100kW':

    while (time.time() < end_time) and (dcdc.getOutputVoltages()[0] < 50.00):

        time.sleep(0.1)

else:

    while ((time.time() < end_time) and ((dcdc.getOutputVoltages()[0] and dcdc.getOutputVoltages()[1]) < 50.00)):

        time.sleep(0.1)

dcdc.setOutputEnabled(False)

tenma.tenma_disable_output(lv_psu)

tenma.tenma_release_device(lv_psu)

dyno.setInverterEnabled(False)

dyno.setDriveEnabled(False)
```

##### Save
Once the flag of test complete is True, the results are stored in a CSV, alongside the alerts.
```python
if test_complete is True:

    df = pd.DataFrame(averageDataCaptured)

    df.to_csv(os.getcwd() + "\\" + TEST_NAME + '.csv', header=True, index=False)

df_alert = pd.DataFrame(fault_dict)

df_alert.to_csv(os.getcwd() + "\\" + "" + TEST_NAME + "_ALERTS" + '.csv', header=True, index=False)
```
