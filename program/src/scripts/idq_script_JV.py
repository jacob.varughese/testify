Iq_poke_address       = 0x003F9610
Id_poke_address       = 0x003F8E0A

fixedpoint_scale_d    = int(65535/1110.72)
fixedpoint_scale_q    = int(65535/1091.72)    

#
#DEMAND_TIME           = [1.0, 1.0]
#LOG_POINT             = [True, True]
# >>> print ("0x%08x" % 123)
# 0x0000007b
#

#------------------------Torque Sense constants----------------------

devices_found = ctypes.c_ulong(0)
first_device = ctypes.c_ulong(0)  
SEARCH_ALL = ctypes.c_ulong(0)
TRUE = ctypes.c_bool(True)
FALSE = ctypes.c_bool(False)
measuredTorque = ctypes.c_float(0.0)
measuredTorqueFilter = ctypes.c_ulong(0)
measuredSpeed = ctypes.c_ulong(0)
measuredSpeedFilter = ctypes.c_ulong(0)

#------------------ Defining Poke function on GEN 4 ------------------
def poke16 (address, offset, value, fixedpoint_scale):                                                # poke16 (varfind_address, offset, injection value, fixedpoint scaling)
    poke_addr = node.sdo[0x5603][3]
    poke_val = node.sdo[0x5603][4] 
    #poke_addr.write("{}".format("0x%08x" % address+offset))
    poke_addr.write(address+offset)
    #if poke_addr.
    #poke_val.write("{}".format("0x%04x" % value))  
    poke_val.write("{}".format(value * fixedpoint_scale))    
    #node.sdo.download(0x5603, 3, b'\x00\x3F\x8E\x0A')
    return[True]

#------------------ Defining function to readback TPDO signals --------------------
def print_speed(message):
    print('%s received' % message.name)
    for var in message:
       # print('%s = %d' % (var.name, (var.raw/16)))
      # print('%s =' % var.name, "%0.4f" % np.right_shift(var.raw, 4) )
       print('%s =' % var.name, "%0.4f" % (var.raw/16) )                        # scaled down by 16 as the messages read-back are x16 bigger


#------------------ CANopen and IXXAT configuration ------------------ 

network = canopen.Network()
network.connect(bustype='ixxat', channel=0, bitrate=500000)


# This will attempt to read an SDO from nodes 1 - 127
network.scanner.search()
# We may need to wait a short while here to allow all nodes to respond
time.sleep(0.05)
for node_id in network.scanner.nodes:
   # print("Found node %d!" % node_id)
    print(f"Found node {network.scanner.nodes[0]}")
print(network.scanner.nodes[0])
node = network.add_node(network.scanner.nodes[0], 'C:\DVT\common\program\EDS\Gen4_pc0x0705302d_rev0x0001002a.eds',False)
time.sleep(1.0)
#--------Startup the TorqSens trasnducer ---------------------

find_device_status = ts.transducerFind(ctypes.byref(devices_found), SEARCH_ALL, 0,TRUE)
time.sleep(2)

print("devices found ", devices_found.value)
while find_device_status == ts.Status.SEARCH_IN_PROGRESS.value:
    print("status is ", find_device_status)
#devices_found.value = 1                 # forcing it to satisfy the if condition
if (find_device_status == ts.Status.SUCCESS.value) and (devices_found.value > 0) :
    print("Found Device            : {}".format(find_device_status), "SUCCESS.. WOOHOO!! ")
    print("Number of Devices         : {}".format(devices_found.value))
    ts.transducerOpen(first_device)
    ts.torqueZeroAverage(first_device)
    ts.speedGetFilter(first_device, measuredSpeedFilter)
    ts.torqueGetFilter(first_device, measuredTorqueFilter)
    
    
    
   
#    print("Speed                     : {0:.3f} rpm".format(measuredSpeed.value))
#    torqueGet(first_device, measuredTorque)
#    print("Torque                    : {0:.3f} Nm".format(measuredTorque.value))
#    transducerClose(first_device, FALSE)
    
    
    
    print("Speed Filter Val          : {}".format(measuredSpeedFilter.value))
    print("Torque Filter Val          : {}".format(measuredTorqueFilter.value))
#------------------ SDO_WNX TO GEN 4 for Idq injection ------------------
# IdIq injection and acquire transducer measurement  
    try:
        for Iq_val in Iq_target:
            poke16(Iq_poke_address, 0, int(Iq_val),fixedpoint_scale_q)
            ts.speedGetFast(first_device, measuredSpeed)
            time.sleep(1.5)
            for Id_val in Id_target:
                poke16(Id_poke_address, 0, int(Id_val),fixedpoint_scale_d)
                ts.torqueGet(first_device, measuredTorque)
                print(f"ID TARGET: {Id_val}, IQ TARGET :{Iq_val}, Torque output : {format(measuredTorque.value)}, Speed output : {format(measuredSpeed.value)}")
                time.sleep(2)
            
    except Exception as e:
        print(e)
    except KeyboardInterrupt:
        poke16(Iq_poke_address, 0, 0)
        poke16(Id_poke_address, 0, 0)           
        
#-------------------------end of Idq injection--------------------------------
else:
    print("Transducer not detected ")


ts.transducerClose(first_device, FALSE)    
poke16(Iq_poke_address, 0, 0, 0)
poke16(Id_poke_address, 0, 0, 0)    
    

#---------------- Reading back signals using PDO's -------------------------

node.tpdo.read()

#def print_speed(message):
#    print('%s received' % message.name)
#    for var in message:
#       # print('%s = %d' % (var.name, (var.raw/16)))
#      # print('%s =' % var.name, "%0.4f" % np.right_shift(var.raw, 4) )
#       print('%s =' % var.name, "%0.4f" % (var.raw/16) )                        # scaled down by 16 as the messages read-back are x16 bigger
#
node.tpdo[1][1].add_callback(print_speed)
time.sleep(1)



#format("0x%04x" % value))


#sw = node.sdo[0x100A]

#print("sw ver is %s" % sw.read())

#Npp = node.sdo.upload(0x4641, 5)

#print("Npp is %s" % Npp)

#poke16(Iq_poke_address, 0, 2)


#print (" length is %d" % len(Iq_target))

# try:
#     for Iq_count in range(len(Iq_target)):
#         poke16(Iq_poke_address, 0,int(Iq_target[Iq_count]))
#         time.sleep(1.5)
#         for Id_count in range(len(Id_target)):
#             poke16(Id_poke_address, 0, int(Id_target[Id_count]))
#             print(f"ID TARGET: {Id_count}, IQ TARGET :{Iq_count}")
#             time.sleep(2)






#if (ts.transducerOpen(first_device)):
#    try:
#    for Iq_val in Iq_target:
#        poke16(Iq_poke_address, 0, int(Iq_val))
#        time.sleep(1.5)
#        for Id_val in Id_target:
#            poke16(Id_poke_address, 0, int(Id_val))
#            print(f"ID TARGET: {Id_val}, IQ TARGET :{Iq_val}")
#            time.sleep(2)
#            
#except Exception as e:
#    print(e)
#except KeyboardInterrupt:
#    poke16(Iq_poke_address, 0, 0)
#    poke16(Id_poke_address, 0, 0)  
#
#poke16(Iq_poke_address, 0, 0)
#poke16(Id_poke_address, 0, 0)           










 