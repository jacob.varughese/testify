# ------------------ CONSTANTS  ------------------
SPEED_OFFSET_ALLOWED = 10 * GEARBOX_RATIO
FLT_ZERO = 0.0
INT_ZERO = 0
TEST_COUNT = 0
LV_PSU_VOLTAGE = 16.00
LOGIN_TIMER = 12

# ------------------ VARIABLES  ------------------
test_fail = False
dcdc = None
mcu = None
dyno = None
lv_psu = None
logger = None

current_sensor_latency_x_ID = [
    0x401ba001,
    0x401ba011,
    0x401ba021,
    0x401ba031,
    0x401ba041,
    0x401ba051,
    0x401ba061,
    0x401ba071,
    0x401ba081,
    0x401ba091,
    0x401ba0a1,
    0x401ba0b1,
    0x401ba0c1,
    0x401ba0d1,
    0x401ba0e1,
    0x401ba0f1,
    0x401ba101
]
current_sensor_latency_y_ID = [
    0x401ba002,
    0x401ba012,
    0x401ba022,
    0x401ba032,
    0x401ba042,
    0x401ba052,
    0x401ba062,
    0x401ba072,
    0x401ba082,
    0x401ba092,
    0x401ba0a2,
    0x401ba0b2,
    0x401ba0c2,
    0x401ba0d2,
    0x401ba0e2,
    0x401ba0f2,
    0x401ba102
]

# ------------------ DICTIONARIES  ------------------
paths = {
"measured" : (os.getcwd() + "\\" + TEST_NAME + '.csv'),
"Alert" : (os.getcwd() + "\\"  + TEST_NAME + "_ALERTS" + '.csv'),
"inverter" : (os.getcwd() + "\\" + TEST_NAME + "_INVERTER_PROPERTIES" '.csv') ,
"current" : (os.getcwd() + "\\" + TEST_NAME + "_CURRENT_SENSOR_LATENCY_TABLE" '.csv')
}

soft_IDs = {
    "Revision": {"ID":0x40150001,"type":"uint"},
    "Ke": {"ID":0x40570017,"type":"float"},
    "Offset": {"ID":0x20180015,"type":"float"},
    "PolePairs": {"ID":0x4057001D,"type":"uint"},
    "BuildTime": {"ID":0x40154000,"type":"uint"},
    "BuildName": {"ID":0x40151000,"type":"string"},
    "IOPCRC": {"ID":0x20150000,"type":"uint"},
    "MCPCRC": {"ID":0x40150000,"type":"uint"},
    "MCPSerial": {"ID":0x40158000,"type":"uint"},
    "IOPSerial": {"ID":0x20158000,"type":"uint"},
}

data_avg = {
    'Test Point':None,
    'DC Link Voltage':None,
    'DC Link Current':None,
    'Torque Measured [Nm]':None,
    'Speed Limit Forward [rpm]':None,
    'Speed Limit Reverse [rpm]':None,
    'Speed eMotor [rpm]':None,
    'Speed eMotor [rad/s]':None,
    'Speed Measured [rpm]':None,
    'Speed Measured [rad/s]':None,
    'Mechanical Power [W]':None,
    'Id Current Injected':None,
    'Id Current Feedback':None,
    'Iq Current Injected':None,
    'Iq Current Feedback':None,
    'Is Current Injected':None,
    'Is Current Feedback':None,
    'Is Current Injected Angle [Degrees]':None,
    'Is Current Feedback Angle [Degrees]':None,
    'Is Current Injected Angle [Radians]':None,
    'Is Current Feedback Angle [Radians]':None,
    'Ud Voltage Feedback':None,
    'Uq Voltage Feedback':None,
    'Motor Stator Temperature [C]':None,
    'Inverter Temperature [C]':None
}


# ------------------ CTYPES  ------------------
FIRST_DEVICE = ctypes.c_ulong(0)
FILTER_USB = ctypes.c_ulong(4)
TRUE = ctypes.c_bool(True)
FALSE = ctypes.c_bool(False)

devices_found = ctypes.c_ulong(0)
measuredTorque = ctypes.c_float(0.0)
measuredSpeed = ctypes.c_ulong(0)

# ------------------ ENUMS ------------------
class InverterState(Enum):
    '''
    Inverter Bridge State
    '''
    TORQUE_ENABLED = 0
    ACTIVE_SHORT_CIRCUIT = 1
    ACTIVE_DISCHARGE = 2
    TEST = 3
    FAULT = 4
    HIGH_IMPEDANCE = 5

# ------------------ FUNCTIONS  ------------------
def save_to_csv(data_dict, path):
    df = pd.DataFrame([data_dict])
    df.to_csv(path, mode='a', header=not os.path.exists(path), index=False)

def get_soft_data(soft_IDs):
    '''
    Get inverter software properties.
    '''
    soft_dict = {
        "Revision" : None,
        "Ke" : None ,
        "Offset" : None,
        "Pole Pairs": None,
        "Build Time" : None,
        "Build Name" : None,
        "IOP CRC" : None,
        "MCP CRC" : None,
        "MCP Serial" : None,
        "IOP Serial" : None
    }

    soft_dict["Revision"]=hex(mcu.readBaseCommsProperty(propertyId=soft_IDs["Revision"]["ID"], propertyType = soft_IDs["Revision"]["type"]))
    soft_dict["Ke"]=mcu.readBaseCommsProperty(propertyId=soft_IDs["Ke"]["ID"], propertyType = soft_IDs["Ke"]["type"])
    soft_dict["Offset"]=mcu.readBaseCommsProperty(propertyId=soft_IDs["Offset"]["ID"], propertyType = soft_IDs["Offset"]["type"])
    soft_dict["PolePairs"]=mcu.readBaseCommsProperty(propertyId=soft_IDs["PolePairs"]["ID"], propertyType = soft_IDs["PolePairs"]["type"])
    soft_dict["BuildTime"]=mcu.readBaseCommsProperty(propertyId=soft_IDs["BuildTime"]["ID"],propertyType=soft_IDs["BuildTime"]["type"])
    soft_dict["BuildName"]=mcu.readBaseCommsProperty(propertyId=soft_IDs["BuildName"]["ID"], propertyType = soft_IDs["BuildName"]["type"])
    soft_dict["IOPCRC"]=hex(mcu.readBaseCommsProperty(propertyId=soft_IDs["IOPCRC"]["ID"], propertyType = soft_IDs["IOPCRC"]["type"]))
    soft_dict["MCPCRC"]=hex(mcu.readBaseCommsProperty(propertyId=soft_IDs["MCPCRC"]["ID"], propertyType = soft_IDs["MCPCRC"]["type"]))
    soft_dict["MCPSerial"]=mcu.readBaseCommsProperty(propertyId=soft_IDs["MCPSerial"]["ID"], propertyType = soft_IDs["MCPSerial"]["type"])
    soft_dict["IOPSerial"]=mcu.readBaseCommsProperty(propertyId=soft_IDs["IOPSerial"]["ID"], propertyType = soft_IDs["IOPSerial"]["type"])

    return soft_dict

def get_current_sensor_latency(x_ID, y_ID, path):
    '''
    Read inverter current sensor latency table
    '''
    current_sensor_dict = {
    "Speed":[],
    "Trim Angle":[]
    }

    for x_ids in x_ID:
        current_sensor_dict["Speed"].append(mcu.readBaseCommsProperty(propertyId =x_ids, propertyType="float"))
    for y_ids in y_ID:
        current_sensor_dict["Trim Angle"].append(mcu.readBaseCommsProperty(propertyId =y_ids, propertyType="float"))

    current_sensor_df = pd.DataFrame(current_sensor_dict)
    current_sensor_df.to_csv(path, index=False)

def average_torque_transducer():
    '''
    Use the 3 sample average window and print outputs
    '''
    lg.info("")
    lg.info(" Zeroing Transducer")
    time.sleep(1.0)
    ts.torqueZeroAverage(FIRST_DEVICE)
    ts.speedGetFast(FIRST_DEVICE, measuredSpeed)
    ts.torqueGet(FIRST_DEVICE, measuredTorque)
    time.sleep(0.5)
    lg.info(f"Transducer Speed: {(measuredSpeed.value * GEARBOX_RATIO):.3f}")
    lg.info(f"Transducer Torque: {(measuredTorque.value / GEARBOX_RATIO):.3f}")
    lg.success("Zeroing Complete")

def MotorTempDwell(UpperTempThreshold, LowerTempTreshold, MotorTempIdqStep, IdInjID, IqInjID, IqInjTarget, IdInjTarget):
    '''
    Temperature Dwell: If temp above threshold , take off current and wait to cool down.
    '''
    iq_val = FLT_ZERO
    id_val = FLT_ZERO

    while mcu.getMotorTemperature() >=UpperTempThreshold:
        if mcu.getMotorTemperature() >= UpperTempThreshold:
            lg.warning(f"Motor Temperature {mcu.getMotorTemperature()}C is above that defined by the Test Threshold {UpperTempThreshold}C, pausing Test")

            while mcu.getMotorTemperature() >= UpperTempThreshold:

                while abs(mcu.readBaseCommsProperty(IdInjID, propertyType="float")) or abs(mcu.readBaseCommsProperty(IqInjID, propertyType="float")) > FLT_ZERO:
                    lg.warning(f"Reducing Current by {MotorTempIdqStep}A steps in the motor to allow the motor to cool. {mcu.getMotorTemperature()}C")

                    if abs(mcu.readBaseCommsProperty(IdInjID, propertyType="float")) - abs(MotorTempIdqStep) <= FLT_ZERO:
                        mcu.writeBaseCommsProperty(IdInjID, FLT_ZERO, propertyType="float")
                        lg.info(f"ID INJECTED WRITE       : {FLT_ZERO:.3f} A")

                    if abs(mcu.readBaseCommsProperty(IqInjID, propertyType="float")) - abs(MotorTempIdqStep) <= FLT_ZERO:
                        mcu.writeBaseCommsProperty(IqInjID, FLT_ZERO, propertyType="float")
                        lg.info(f"IQ INJECTED WRITE       : {FLT_ZERO:.3f} A")

                    if abs(mcu.readBaseCommsProperty(IdInjID, propertyType="float")) - abs(MotorTempIdqStep) > FLT_ZERO:
                        id_val = mcu.readBaseCommsProperty(IdInjID, propertyType="float") + abs(MotorTempIdqStep)
                        mcu.writeBaseCommsProperty(IdInjID, id_val, propertyType="float")
                        lg.info(f"ID INJECTED WRITE       : {id_val:.3f} A")

                    if abs(mcu.readBaseCommsProperty(IqInjID, propertyType="float")) - abs(MotorTempIdqStep) > FLT_ZERO:
                        iq_val = mcu.readBaseCommsProperty(IqInjID, propertyType="float") + (abs(MotorTempIdqStep) * np.sign(mcu.readBaseCommsProperty(IqInjID, propertyType="float")) * -1.0)
                        mcu.writeBaseCommsProperty(IqInjID, iq_val, propertyType="float")
                        lg.info(f"IQ INJECTED WRITE       : {iq_val:.3f} A")

                    time.sleep(0.5)
                    id_read = mcu.readBaseCommsProperty(IdInjID, propertyType="float")
                    iq_read = mcu.readBaseCommsProperty(IqInjID, propertyType="float")
                    lg.info(f"ID INJECTED READ        : {id_read:.3f} A")
                    lg.info(f"IQ INJECTED READ        : {iq_read:.3f} A")

                while mcu.getMotorTemperature() > LowerTempTreshold:

                    lg.warning(f"Waiting for Motor Temperature {mcu.getMotorTemperature()}C to drop to Lower Temperature Threshold {LowerTempTreshold}C")

                    mcu.writeBaseCommsProperty(IdInjID, FLT_ZERO, propertyType= "float")
                    mcu.writeBaseCommsProperty(IqInjID, FLT_ZERO, propertyType= "float")

                    lg.info(f"ID INJECTED WRITE       : {FLT_ZERO:.3f} A")
                    lg.info(f"IQ INJECTED WRITE       : {FLT_ZERO:.3f} A")

                    time.sleep(1)

                    id_read = mcu.readBaseCommsProperty(IdInjID, propertyType="float")
                    iq_read = mcu.readBaseCommsProperty(IqInjID, propertyType="float")
                    lg.info(f"ID INJECTED READ        : {id_read:.3f} A")
                    lg.info(f"IQ INJECTED READ        : {iq_read:.3f} A")

        mcu.writeBaseCommsProperty(IdInjID, FLT_ZERO, propertyType= "float")
        mcu.writeBaseCommsProperty(IqInjID, FLT_ZERO, propertyType= "float")

        if mcu.getMotorTemperature() <= LowerTempTreshold:
            lg.warning("Increasing Current in the motor to allow the test to continue.")

            while mcu.getMotorTemperature() < UpperTempThreshold:

                while (abs(mcu.readBaseCommsProperty(IqInjID, propertyType="float")) < abs(IqInjTarget)) or (abs(mcu.readBaseCommsProperty(IdInjID, propertyType="float")) < abs(IdInjTarget)):

                    if mcu.getMotorTemperature() >= UpperTempThreshold:
                        break

                    if abs(mcu.readBaseCommsProperty(IdInjID, propertyType="float")) + abs(MotorTempIdqStep) <= abs(IdInjTarget):
                        id_val = mcu.readBaseCommsProperty(IdInjID, propertyType="float") - abs(MotorTempIdqStep)
                        mcu.writeBaseCommsProperty(IdInjID, id_val, propertyType="float")
                        lg.info(f"ID INJECTED WRITE       : {id_val:.3f} A")

                    if abs(mcu.readBaseCommsProperty(IqInjID, propertyType="float")) + abs(MotorTempIdqStep) <= abs(IqInjTarget):
                        iq_val = mcu.readBaseCommsProperty(IqInjID, propertyType="float") + (MotorTempIdqStep  * np.sign(IqInjTarget))
                        mcu.writeBaseCommsProperty(IqInjID, iq_val, propertyType="float")
                        lg.info(f"IQ INJECTED WRITE       : {iq_val:.3f} A")

                    if abs(mcu.readBaseCommsProperty(IdInjID, propertyType= "float")) + abs(MotorTempIdqStep) > abs(IdInjTarget):
                        mcu.writeBaseCommsProperty(IdInjID, float(IdInjTarget), propertyType="float")
                        lg.info(f"ID INJECTED WRITE       : {float(IdInjTarget):.3f} A")

                    if abs(mcu.readBaseCommsProperty(IqInjID, propertyType= "float")) + abs(MotorTempIdqStep) > abs(IqInjTarget):
                        mcu.writeBaseCommsProperty(IqInjID, float(IqInjTarget), propertyType="float")
                        lg.info(f"IQ INJECTED WRITE       : {float(IqInjTarget):.3f} A")

                    time.sleep(0.5)
                    id_read = mcu.readBaseCommsProperty(IdInjID, propertyType="float")
                    iq_read = mcu.readBaseCommsProperty(IqInjID, propertyType="float")
                    lg.info(f"ID INJECTED READ        : {id_read:.3f} A")
                    lg.info(f"IQ INJECTED READ        : {iq_read:.3f} A")

                if (abs(mcu.readBaseCommsProperty(IqInjID, propertyType="float")) == abs(float(IqInjTarget))) and (abs(mcu.readBaseCommsProperty(IdInjID, propertyType="float")) == abs(IdInjTarget)):
                    id_read = mcu.readBaseCommsProperty(IdInjID, propertyType="float")
                    iq_read = mcu.readBaseCommsProperty(IqInjID, propertyType="float")
                    lg.info(f"Motor Temperature {mcu.getMotorTemperature():.3f}C is less than the Upper Limit {UpperTempThreshold:.3f}C and IDQ current demands are reloaded: ID {id_read:.3f}A, IQ {iq_read:.3f}A")
                    break

# ------------------ logger  ------------------
LOG_FORMAT = "{time:YYYY-MM-DD at HH:mm:ss} | {level} | {message}"
LOG_NAME = "console_out.log"
lg.add(LOG_NAME, level="INFO", format=LOG_FORMAT, mode='w')

# ------------------ START OF SCRIPT  ------------------
lg.info("")
lg.info("##################################")
lg.info("[       Idq Injection Script     ]")
lg.info("##################################")
lg.info("")

while True:
    lg.warning("The Idq Injection script is about to start.")
    lg.warning("Make sure all cooling and saftey procedures have been followed.")
    lg.warning("Would you like to proceed? ")
    # some code here
    proceed = input("")
    if proceed not in ["yes", "Yes", "Y", "y", "YES"]:
        lg.info("")
        lg.error("Exiting the program")
        test_fail = True
        exit()
    else:
        lg.info("")
        lg.info("You Entered: " + str(proceed))
        break

# Start MunchPy
lg.info("")
lg.warning("Initialising MunchPy")

munch_interop = mpy.MunchInterop.getInstance()
if (munch_interop is not None) and (test_fail is False):
    munch_interop.initialise(
        loggingPath=LOGGING_PATH,
        keepCandiAlive=True
    )
    lg.info(f"MunchPy Logging Path: {LOGGING_PATH}")
    lg.success("MunchPy Initialised")

    lg.info("")

    try:
        lg.warning("Setting Up MunchPy")
        # Setup HW interfaces
        
        if munch_interop.getClient().connect():
            time.sleep(1)

            MCUCanDevice = mpy.CANDevice.getSpecific(
                munchInterop=munch_interop,
                name=CAN_HWID_MCU
            )
            lg.info(f"MCU CAN Device: {CAN_HWID_MCU}")

            DCDCCanDevice = mpy.CANDevice.getSpecific(
                munchInterop=munch_interop,
                name=CAN_HWID_DCDC
            )
            lg.info(f"DCDC CAN Device: {CAN_HWID_DCDC}")

            if DYNO_LOCATION == 'DX: 100kW':
                dcdc = mpy.SingleDCDC(
                    munchInterop=munch_interop,
                    canDevice=DCDCCanDevice
                )
                lg.info(f"DCDC attached to Single Class")
                
            else:
                dcdc = mpy.ParallelDCDC(
                    munchInterop=munch_interop,
                    canDevice=DCDCCanDevice
                )
                lg.info(f"DCDC attached to Parallel Class")

            mcu = mpy.Derwent(
                munchInterop=munch_interop,
                canDevice=MCUCanDevice
            )
            lg.info(f"MCU attached to Derwent Class")

            dyno = mpy.NidecDyno(
                munchInterop=munch_interop,
                ipAddress=DYNO_IP,
                ipPort=DYNO_PORT,
                unitId=DYNO_ID
            )
            lg.info(f"Dyno attached to Nidec Class")

            logger = mpy.Logger(munch_interop)

            lg.success("MunchPy Setup Complete")

            lg.info("")

            # Tenma enable
            lg.warning("Enabling Tenma")
            lv_psu = tenma.get_device()

            if test_fail is False:
                try:
                    tenma.key_on(lv_psu)
                    lg.success("Tenma Enabled")
                except Exception as e:
                    lg.warning(e)
                    test_fail = True

            lg.info("")

            lg.warning("Initlialising Torque Sensor")

            if test_fail is False and TORQUE_SENSOR == 'TS':
                # Find transducer connected.
                try:
                    find_device_status = ts.transducerFind(ctypes.byref(devices_found), FILTER_USB, 0, TRUE)
                    ts.transducerOpen(FIRST_DEVICE)
                    ts.torqueSetFilter(FIRST_DEVICE, ctypes.c_ulong(TORQUE_SENSOR_TORQUE_FILTER), ctypes.c_bool(False))

                except Exception as e:
                    test_fail = True
                    lg.critical(e)
                # If a device is found and the return is successful.
                if (find_device_status == ts.Status.SUCCESS.value) and (devices_found.value > 0):
                    lg.info("Torque Sensor Found: TS")
                    lg.success("Torque Sensor Initialised")
                else:
                    lg.error("Devices not found")
                    test_fail = True
            
            lg.info("")

            if abs(dyno.getSpeedFeedback()) > 5.0:
                lg.warning("Dyno Rotation Detected, setting 0 rpm")
                dyno.setPresetReference1(FLT_ZERO)
                while abs(dyno.getSpeedFeedback()) > 5.0:
                    lg.info(f"Dyno Speed {dyno.getSpeedFeedback():.3f} rpm, Target Speed: {FLT_ZERO:.3f} rpm")
                    time.sleep(1.0)
                lg.info("")

            # Inverter Login and enable IDQ Test Mode with Equal Axis Priority
            if test_fail is False:
                lg.warning("Logging Into MCU")
                while LOGIN_TIMER > 0:
                    lg.info(f"Waiting for Login timer to expire {LOGIN_TIMER} seconds")
                    time.sleep(1)
                    LOGIN_TIMER = LOGIN_TIMER - 1

                lg.info("Logging Into BaseComms")
                try:
                    mcu.baseCommsLogin(password=PASSWORD)
                    lg.info("Successfully Logged into Basecomms")
                    time.sleep(0.1)
                    lg.success("Logged into MCU")
                except Exception as e:
                    lg.critical("Could not log into MCU")
                    lg.critical(e)
                    test_fail = True

                lg.info("")
                lg.warning("Reading Inverter Properties")
                try:
                    soft_data = get_soft_data(soft_IDs)
                    save_to_csv(data_dict = soft_data, path = paths["inverter"])
                    lg.success("Inverter Properties Read and Saved")

                except Exception as e:
                    lg.error("Could not read Inverter Properties")
                    lg.error(e)
                    
                lg.info("")
                lg.warning("Reading Current Sensor Latency")
                try:
                    get_current_sensor_latency(
                        x_ID = current_sensor_latency_x_ID,
                        y_ID = current_sensor_latency_y_ID,
                        path = paths["current"]
                    )

                    lg.success("Current Sensor Latency Table Read and Saved")

                except Exception as e:
                    lg.error("Could not read Current Sensor Latency Table")
                    lg.error(e)
                    
                lg.info("")

            # Configure Test Mode for IDQ
            lg.warning("Configuring Test Mode for Idq Injection")
            lg.info("Writing BaseComm Properties for requested Idq Mode and Axis ...")
            
            if test_fail is False:
                try:
                    mcu.writeBaseCommsProperty(TEST_MODE_ID, TMODE_MODE_VAL, propertyType="float")
                    mcu.writeBaseCommsProperty(TMODE_AXIS_ID, TMODE_AXIS_VAL, propertyType="float")
                    time.sleep(0.2)
                    
                    lg.success("Idq Test Mode BaseComm Properties Written")
                except Exception as e:
                    lg.critical(e)
                    lg.critical("Quitting")
                    test_fail = True

            # Configure Test Mode for IDQ
            if test_fail is False:
                lg.warning("Configuring MCU Current Limits")
                try:
                    lg.info(f"Setting [+]: {MCU_DISCHARGE_I_LIMIT:.3f}A, [-]: {MCU_CHARGE_I_LIMIT:.3f}A")
                    mcu.setCurrentLimits(
                        dischargeCurrent=MCU_DISCHARGE_I_LIMIT,
                        rechargeCurrent=MCU_CHARGE_I_LIMIT
                    )

                    time.sleep(0.1)
                    lg.success("MCU Limits Written")
                except Exception as e:
                    lg.error(e)
                    lg.error("Quitting")
                    test_fail = True

            lg.info("")

            # dcdc enable
            if test_fail is False:
                lg.warning("Enabling DCDC")
                try:

                    lg.info(f"DCDC Voltage Target: {DCDC_V_TARGET:.3f} V")
                    lg.info(f"DCDC Current Limits: +{DCDC_I_LIMIT_POS:.3f} A, {DCDC_I_LIMIT_NEG:.3f} A")
                    
                    dcdc.setOutputCurrentLimits(
                        positiveCurrent=DCDC_I_LIMIT_POS,
                        negativeCurrent=DCDC_I_LIMIT_NEG
                    )

                    time.sleep(0.2)
                    dcdc.setOutputEnabled(True)
                    time.sleep(0.2)
                    dcdc.setOutputVoltage(DCDC_V_TARGET)

                    end_time = time.time() + 5
                    lg.info("DCDC Enabled")
                    if DYNO_LOCATION == 'DX: 100kW':
                        while (time.time() < end_time) and (dcdc.getOutputVoltages()[0] < (DCDC_V_TARGET * 0.98)):

                            lg.info(f"DCDC Output Voltage: {dcdc.getOutputVoltages()[0]:.3f}V, Target: {DCDC_V_TARGET:.3f}V")
                            time.sleep(0.1)

                            if time.time() > end_time and (dcdc.getOutputVoltages()[0] < (DCDC_V_TARGET * 0.50)):
                                dcdc.setOutputVoltage(FLT_ZERO)
                                test_fail = True
                                break

                    else:
                        while ((time.time() < end_time) and ((dcdc.getOutputVoltages()[0] and dcdc.getOutputVoltages()[1]) < DCDC_V_TARGET * 0.98)):
                            lg.info(f"DCDC Output Voltage[0]: {dcdc.getOutputVoltages()[0]:.3f}V, DCDC Output Voltage[0]: {dcdc.getOutputVoltages()[1]:.3f}V,  Target: {DCDC_V_TARGET:.3f}V")
                            time.sleep(0.1)

                            if time.time() > end_time and (dcdc.getOutputVoltages()[0] < (DCDC_V_TARGET * 0.50)):
                                dcdc.setOutputVoltage(FLT_ZERO)
                                test_fail = True
                                break
                    if test_fail is False:
                        lg.success("DCDC Enabled and at the target Voltage")
                    else:
                        lg.critical("DCDC Has not reached Target Voltage")
                except Exception as e:
                    lg.error(e)

            lg.info("")

            # dyno enable
            
            if test_fail is False:
                lg.warning("Enabling Dyno")
                try:
                    dyno.setDriveEnabled(True)
                    time.sleep(0.5)
                    lg.info("Dyno Drive Enabled")
                    dyno.setInverterEnabled(True)
                    time.sleep(0.5)
                    lg.info("Dyno Inverter Enabled")
                    lg.success("Dyno Enabled")
                except Exception as e:
                    lg.info(e)

            lg.info("")

            # Enable mcu Bridge
            
            if test_fail is False:
                lg.warning("Enabling MCU Bridge")
                try:
                    mcu.setBridgeEnabled(True)
                    time.sleep(1)
                    lg.success("MCU Bridge Enabled")

                    logger.beginLogging(LOGGING_PATH, TEST_NAME)

                    lg.info("")
                    lg.info("----------------------------------------------------------")
                    lg.info("~~~~~~~~~~~~~~~~~~~~~ Test Started ~~~~~~~~~~~~~~~~~~~~~~~")
                    lg.info("----------------------------------------------------------")
                    lg.info("")
                    time.sleep(2)

                    init_zero = False
                    dyno.setMaximumReferenceClamp(max(SPEED_DEMANDS) * 1.2 / GEARBOX_RATIO)
                    for step in range(len(SPEED_DEMANDS)):
                        # Maybe better as a while loop?
                        if mcu.getBridgeState() != 0:
                            lg.critical(f"BRIDGE STATE NOT {InverterState(0).name} : CURRENT STATE = {InverterState(mcu.getBridgeState()).name}")
                            mcu.writeBaseCommsProperty(IQ_INJ_ID, FLT_ZERO, propertyType="float")
                            mcu.writeBaseCommsProperty(ID_INJ_ID, FLT_ZERO, propertyType="float")

                            time.sleep(0.1)
                            dcdc.setOutputVoltage(FLT_ZERO)
                            time.sleep(0.1)
                            dyno.setPresetReference1(FLT_ZERO)

                            fault_dict = {
                                'Alert ID': hex(int(mcu.getAlertID())),
                                'Block ID': hex(int(mcu.getBlockID()))
                            }

                            save_to_csv(data_dict = fault_dict, path = paths["Alert"])  

                            test_fail = True
                            
                            break

                        dyno.setPresetReference1(SPEED_DEMANDS[step] / GEARBOX_RATIO)

                        end_time = time.time() + 5

                        while (time.time() < end_time) and not ((abs(SPEED_DEMANDS[step]) - abs(SPEED_OFFSET_ALLOWED)) <= abs(mcu.getSpeed()) <= (abs(SPEED_DEMANDS[step]) + abs(SPEED_OFFSET_ALLOWED))):
                            time.sleep(0.1)

                        if ((abs(SPEED_DEMANDS[step]) - abs(SPEED_OFFSET_ALLOWED)) <= abs(mcu.getSpeed()) <= (abs(SPEED_DEMANDS[step]) + abs(SPEED_OFFSET_ALLOWED))):

                            if init_zero is False:
                                average_torque_transducer()
                                triggerTransducerZero = False
                                init_zero = True

                            if triggerTransducerZero is True:
                                time.sleep(0.5)
                                average_torque_transducer()
                                triggerTransducerZero = False

                            lg.info("")
                            lg.info("--------------------Test Point {}/{}--------------------".format(step + 1, len(SPEED_DEMANDS)))
                            lg.info("")
                            lg.info(f"SPEED DEMANDED           [rpm]: {SPEED_DEMANDS[step]:.3f}".format())
                            lg.info(f"CURRENT EMOTOR SPEED     [rpm]: {mcu.getSpeed():.3f}".format())
                            lg.info(f"CURRENT DYNO SPEED       [rpm]: {(dyno.getSpeedFeedback() * GEARBOX_RATIO):.3f}")
                            lg.info(f"ID CURRENT DEMANDED        [A]: {ID_DEMANDS[step]:.3f}")
                            lg.info(f"IQ CURRENT DEMANDED        [A]: {IQ_DEMANDS[step]:.3f}")
                            lg.info(f"IS CURRENT DEMANDED        [A]: {np.sqrt((ID_DEMANDS[step]*ID_DEMANDS[step])+(IQ_DEMANDS[step]*IQ_DEMANDS[step])):.3f}")
                            # Attempt to set speed limits then demand torque.
                            for attempt in range(3):
                                try:
                                    mcu.writeBaseCommsProperty(IQ_INJ_ID, float(IQ_DEMANDS[step]), propertyType= "float")
                                    mcu.writeBaseCommsProperty(ID_INJ_ID, float(ID_DEMANDS[step]), propertyType= "float")
                                    break
                                except Exception as e:
                                    lg.error(e)

                            if MOTOR_DWELL_ENABLE is True:
                                MotorTempDwell(MOTOR_TEMP_UPPER, MOTOR_TEMP_LOWER, MOTOR_TEMP_IDQ_STEP, ID_INJ_ID, IQ_INJ_ID,IQ_DEMANDS[step], ID_DEMANDS[step])

                            # MEASUREMENT
                            # Measurement intialisation
                            if DELAY_LOGGING is True:
                                time.sleep(LOG_DELAY_PERIOD)

                            end_time = time.time() + DEMAND_TIME[step]
                            loop_count= 0
                            dclink_voltage= 0
                            dclink_current= 0
                            iq_feedback=0
                            id_feedback=0
                            ud_feedback=0
                            uq_feedback=0
                            motor_temperature= 0
                            inverter_temperature = 0
                            speed_measured_rpm= 0
                            speed_emotor_rpm=0
                            torque_measured= 0
                            alert_id = 0
                            block_id = 0

                            # Get Measured Signals.
                            while time.time() < end_time:

                                dclink_voltage = dclink_voltage + mcu.getVdc()
                                dclink_current = dclink_current + mcu.getIdc()
                                ts.torqueGet(FIRST_DEVICE, measuredTorque)
                                torque_measured = torque_measured + (measuredTorque.value / GEARBOX_RATIO)
                                ts.speedGetFast(FIRST_DEVICE, measuredSpeed)
                                speed_measured_rpm = speed_measured_rpm + (measuredSpeed.value * GEARBOX_RATIO)
                                speed_emotor_rpm = speed_emotor_rpm + mcu.getSpeed()
                                id_feedback = id_feedback + mcu.getIdCurrentFeedback()
                                iq_feedback = iq_feedback + mcu.getIqCurrentFeedback()
                                ud_feedback = ud_feedback + mcu.getUdVoltageFeedback()
                                uq_feedback = ud_feedback + mcu.getUqVoltageFeedback()
                                motor_temperature = motor_temperature + mcu.getMotorTemperature()
                                inverter_temperature = inverter_temperature + mcu.getInverterTemperature()
    
                                fault_dict = {
                                    'Alert ID': hex(int(mcu.getAlertID())),
                                    'Block ID': hex(int(mcu.getBlockID()))
                                }

                                save_to_csv(data_dict = fault_dict, path = paths["Alert"])  

                                loop_count = loop_count + 1
                                time.sleep(LOOP_SAMPLE_TIME)

                            # Average results
                            data_avg["DC Link Voltage"] = dclink_voltage / loop_count
                            data_avg["DC Link Current"] = dclink_current / loop_count
                            data_avg["Torque Measured [Nm]"] = torque_measured / loop_count
                            data_avg["Speed Measured [rpm]"] = speed_measured_rpm / loop_count
                            data_avg["Speed eMotor [rpm]"] = speed_emotor_rpm / loop_count
                            data_avg["Id Current Feedback"] = id_feedback / loop_count
                            data_avg["Iq Current Feedback"] = iq_feedback / loop_count
                            data_avg["Ud Voltage Feedback"] = ud_feedback / loop_count
                            data_avg["Uq Voltage Feedback"] = uq_feedback / loop_count
                            data_avg["Motor Stator Temperature [C]"] = motor_temperature / loop_count
                            data_avg["Inverter Temperature [C]"] = inverter_temperature / loop_count
                            # Further calculated values
                            data_avg["Test Point"] = step
                            data_avg["Speed Demanded [rpm]"] = SPEED_DEMANDS[step]
                            data_avg["Speed Limit Forward [rpm]"] = SPEED_LIMITS_FORWARD[step]
                            data_avg["Speed Limit Reverse [rpm]"] = SPEED_LIMITS_REVERSE[step]
                            data_avg["Speed eMotor [rad/s]"] = data_avg["Speed eMotor [rpm]"] * np.radians(6)
                            data_avg["Speed Measured [rad/s]"] = data_avg["Speed Measured [rpm]"] * np.radians(6)
                            data_avg["Mechanical Power [W]"] = data_avg["Torque Measured [Nm]"] * data_avg["Speed Measured [rad/s]"]
                            data_avg["Id Current Injected"] = ID_DEMANDS[step]
                            data_avg["Iq Current Injected"] = IQ_DEMANDS[step]
                            data_avg["Is Current Injected"] = np.sqrt((data_avg["Id Current Injected"] * data_avg["Id Current Injected"]) + (data_avg["Iq Current Injected"] * data_avg["Iq Current Injected"]))
                            data_avg["Is Current Injected Angle [Radians]"] = np.arctan2(np.abs(data_avg["Id Current Injected"]), np.abs(data_avg["Iq Current Injected"]))
                            data_avg["Is Current Injected Angle [Degrees]"] = np.rad2deg(data_avg["Is Current Injected Angle [Radians]"])
                            data_avg["Is Current Feedback"] = np.sqrt((data_avg["Id Current Feedback"] * data_avg["Id Current Feedback"]) + (data_avg["Iq Current Feedback"] * data_avg["Iq Current Feedback"]))
                            data_avg["Is Current Feedback Angle [Radians]"] = np.arctan2(np.abs(data_avg["Id Current Injected"]), np.abs(data_avg["Iq Current Feedback"]))
                            data_avg["Is Current Feedback Angle [Degrees]"] = np.rad2deg(data_avg["Is Current Feedback Angle [Radians]"])

                            if int(LOG_POINT[step]) > 0:
                                save_to_csv(data_dict = data_avg, path = paths["measured"])
                            
                            lg.info(pd.Series(data_avg))

                            try:
                                if SPEED_DEMANDS[step] != SPEED_DEMANDS[step + 1]:
                                    triggerTransducerZero = True
                            except IndexError:
                                triggerTransducerZero = True

                        else:
                            lg.critical(f"Failed to reach speed: {SPEED_DEMANDS[step]:.3f} rpm (Current Speed {dyno.getSpeedFeedback():.3f} rpm)")
                            test_fail = True

                        lg.info("")
                        lg.success(f"     End of test point {step + 1}")

                        if test_fail:
                            lg.critical("Test Failed")
                            break

                except Exception as e:
                    lg.critical(e)
                    lg.critical("Bridge failed to enable")

                except KeyboardInterrupt:
                    lg.critical("Keyboard Interrupt Caught")
                    mcu.writeBaseCommsProperty(IQ_INJ_ID, FLT_ZERO, propertyType="float")
                    mcu.writeBaseCommsProperty(ID_INJ_ID, FLT_ZERO, propertyType="float")
                    time.sleep(0.1)
                    dcdc.setOutputVoltage(FLT_ZERO)
                    time.sleep(0.1)
                    dyno.setPresetReference1(FLT_ZERO)

                lg.info("")
                lg.info("----------------------------------------------------------")
                lg.info("~~~~~~~~~~~~~~~~~~~~~ Test Complete ~~~~~~~~~~~~~~~~~~~~~~")
                lg.info("----------------------------------------------------------")
                lg.info("")
                test_complete = True

    except Exception as e:
        lg.critical(e)
        lg.critical("Test Failure")

try:
    lg.info(f"Idq Injection Values to {FLT_ZERO} A")
    mcu.writeBaseCommsProperty(IQ_INJ_ID, FLT_ZERO, propertyType="float")
    mcu.writeBaseCommsProperty(ID_INJ_ID, FLT_ZERO, propertyType="float")
except Exception as e:
    lg.critical(e)

if abs(dyno.getSpeedFeedback()) > 5.0:
    lg.warning(f"Dyno Rotation Detected, setting {FLT_ZERO} rpm")
    try:
        dyno.setPresetReference1(FLT_ZERO)
        while abs(dyno.getSpeedFeedback()) > 5.0:
            lg.info(f"Dyno Speed {dyno.getSpeedFeedback():.3f} rpm, Target Speed: {FLT_ZERO} rpm")
            time.sleep(1.0)
        lg.info("")
    except Exception as e:
        lg.critical(e)

# dcdc disable
lg.info(f"Setting DCDC Voltage Output to {FLT_ZERO} V")
try:
    dcdc.setOutputVoltage(FLT_ZERO)
except Exception as e:
    lg.critical(e)
if DYNO_LOCATION == 'DX: 100kW':
    while (time.time() < end_time) and (dcdc.getOutputVoltages()[0] < 50.00):
        lg.info(f"DCDC Output Voltage: {dcdc.getOutputVoltages()[0]}V, Target: {FLT_ZERO:.3f}V")
        time.sleep(0.1)
else:
    while ((time.time() < end_time) and ((dcdc.getOutputVoltages()[0] and dcdc.getOutputVoltages()[1]) < 50.00)):
        lg.info(f"DCDC Output Voltage[0]: {dcdc.getOutputVoltages()[0]:.3f}V, DCDC Output Voltage[1]: {dcdc.getOutputVoltages()[1]:.3f}V,  Target: {FLT_ZERO:.3f}V")
        time.sleep(0.1)
lg.info("Disabling DCDC")
try:
    dcdc.setOutputEnabled(False)
    lg.success("DCDC Disabled")
except Exception as e:
    lg.critical(e)

# Tenma disable
lg.info("Disable Tenma")
tenma.disable_output(lv_psu)
tenma.release_device(lv_psu)

# dyno disable
try:
    time.sleep(1)
    dyno.setInverterEnabled(False)
    dyno.setDriveEnabled(False)
except Exception as e:
    lg.critical(e)

# release Torque sensor
try:
    ts.transducerClose(FIRST_DEVICE)
except BaseException:
    lg.error("Couldnt close transducer")

#Clso eMunch Py
lg.info("Terminating MunchPy")
try:
    logger.endLogging()
    munch_interop.getClient().disconnect()
    munch_interop.terminate()
    time.sleep(1)
except Exception as e:
    lg.warning(e)

while True:
    lg.warning("Any Entry to Exit")
    finish = input("")
    if finish != "":
        sys.exit()
