import time
import serial
from serial.tools import list_ports

def voltage_target(tenma, v_target = 16.00):
    '''
    Set tenma Psu Voltage
    '''
    tenma.write(str.encode('BEEP1'))
    time.sleep(0.1)
    return tenma.write(str.encode('VSET1:' + str(v_target)))


def enable_output(tenma):
    '''
    Enable tenma psu output
    '''
    time.sleep(0.1)
    return tenma.write(str.encode('OUT1'))


def disable_output(tenma):
    '''
    Disable tenma psu output
    '''
    time.sleep(0.1)
    return tenma.write(str.encode('OUT0'))


def get_device():
    '''
    Search for a tenma psu
    '''
    foundPorts = list(list_ports.comports())
    
    for port in foundPorts:
        tempPort = serial.Serial()
        tempPort.port = port.device
        tempPort.open()

        if tempPort.is_open is True:
            tempPort.write(str.encode('*IDN?'))
            
            if 'TENMA' in str(tempPort.read(5)):
                tenma = tempPort
                time.sleep(0.1)
                break
            else:
                pass

    return tenma


def release_device(tenma):
    '''
    Search for a tenma psu
    '''
    tenma.close()

    return

def key_on(lv_psu, lv_psu_voltage = 16.00):
    voltage_target(lv_psu, 0.0)
    time.sleep(2)
    voltage_target(lv_psu, lv_psu_voltage)
    enable_output(lv_psu)
    time.sleep(1)