import os
import pandas as pd
import numpy as np
from scipy.interpolate import interp2d
import streamlit as st

def torque_speed_ref(gen_cfg):
    '''
    generate torque speed references
    '''
    torque_demand = []
    demand_period = []
    speed_demand = []
    speed_lim_fwd = []
    speed_lim_rev = []
    test_point = []

    # Torque
    torque_max = gen_cfg["Torque"]["Maximum"] * gen_cfg["Torque"]["Direction"]
    torque_min = gen_cfg["Torque"]["Minimum"] * gen_cfg["Torque"]["Direction"]
    torque_step_up = gen_cfg["Torque"]["Ramp Up Value"] * gen_cfg["Torque"]["Direction"]
    torque_step_down = gen_cfg["Torque"]["Ramp Down Value"] * gen_cfg["Torque"]["Direction"]
    torque_demand_period_up = gen_cfg["Torque"]["Ramp Up Period"]
    torque_demand_period_down = gen_cfg["Torque"]["Ramp Down Period"]
    torque_direction = gen_cfg["Torque"]["Direction"]
    torque_skip = gen_cfg["Torque"]["Skip"]
    
    # Speed
    speed_lim_fwd_threshold = gen_cfg["Speed"]["Limit Value"]
    speed_lim_rev_threshold = gen_cfg["Speed"]["Limit Value"]
    speed_lim_method = gen_cfg["Speed"]["Limit Type"]
    speed_step = gen_cfg["Speed"]["Step"]
    speed_direction = gen_cfg["Speed"]["Direction"]
    speed_step_current = gen_cfg["Speed"]["Minimum"] * speed_direction
    speed_max_table = gen_cfg["Speed"]["Max Table"]
    speed_max_requested = gen_cfg["Speed"]["Maximum"]

    # Voltage
    voltage_target = gen_cfg["Voltage"]["Voltage"]

    # Logging
    log_up = gen_cfg["Logging"]["Log Up"]
    log_down = gen_cfg["Logging"]["Log Down"]

    # Interoplation
    interp_function = gen_cfg["Interpolation"]["Function"]

    if speed_lim_method == "Percentage":
        speed_lim_fwd_current = max(speed_step_current * speed_lim_fwd_threshold, 0)
        speed_lim_rev_current = min(speed_step_current * speed_lim_rev_threshold, 0)

    speed_max = min(abs(speed_max_table), abs(speed_max_requested)) * speed_direction

    # Start of Loop
    while abs(speed_step_current) <= abs(speed_max):

        # Get Torque Max value after percentage gain applied
        torque_max_available = interp_function(voltage_target, speed_step_current)
        torque_max_available = torque_max_available[0] * torque_max

        # Every speed step, start torque from minimum torque step
        torque_step_current = torque_min

        if torque_direction > 0:
            # Start torque increment
            while torque_step_current < torque_max_available:

                # Store current values
                torque_demand.append(torque_step_current)
                demand_period.append(torque_demand_period_up)
                speed_demand.append(speed_step_current)
                speed_lim_fwd.append(speed_lim_fwd_current)
                speed_lim_rev.append(speed_lim_rev_current)
                test_point.append(log_up)

                # Step torque up using requested torque step
                torque_step_current = torque_step_current + torque_step_up

                # If torque plus the step is going to be above the amaximum available torque
                # Take a incremenet off for the ramp down, else there would be two points at the maximum torque.
                if torque_step_current >= torque_max_available:
                    torque_step_current = torque_step_current - torque_step_up
                    break

            if (torque_step_current >= torque_max_available) and (torque_skip is False):

                torque_step_current = torque_max_available

                # Store current values
                torque_demand.append(torque_step_current)
                demand_period.append(torque_demand_period_up)
                speed_demand.append(speed_step_current)
                speed_lim_fwd.append(speed_lim_fwd_current)
                speed_lim_rev.append(speed_lim_rev_current)
                test_point.append(log_up)

                # ramp down
            while torque_step_current > torque_min:

                torque_step_current = torque_step_current - torque_step_down

                if torque_step_current <= torque_min:

                    torque_step_current = 0

                    # Store current values
                    torque_demand.append(torque_step_current)
                    demand_period.append(torque_demand_period_down)
                    speed_demand.append(speed_step_current)
                    speed_lim_fwd.append(speed_lim_fwd_current)
                    speed_lim_rev.append(speed_lim_rev_current)
                    test_point.append(log_down)
                    break

                # Store current values
                torque_demand.append(torque_step_current)
                demand_period.append(torque_demand_period_down)
                speed_demand.append(speed_step_current)
                speed_lim_fwd.append(speed_lim_fwd_current)
                speed_lim_rev.append(speed_lim_rev_current)
                test_point.append(log_down)

        if torque_direction < 0:

            # Start torque increment

            while torque_step_current > torque_max_available:
                # Store current values
                torque_demand.append(torque_step_current)
                demand_period.append(torque_demand_period_up)
                speed_demand.append(speed_step_current)
                speed_lim_fwd.append(speed_lim_fwd_current)
                speed_lim_rev.append(speed_lim_rev_current)
                test_point.append(log_up)

                # Step torque up using requested torque step
                torque_step_current = torque_step_current + torque_step_up

                # If torque plus the step is going to be above the amaximum available torque
                # Take a incremenet off for the ramp down, else there would be two points at the maximum torque.
                if torque_step_current <= torque_max_available:
                    torque_step_current = torque_step_current - torque_step_up
                    break

            if (torque_step_current <= torque_max_available) and (torque_skip is False):

                torque_step_current = torque_max_available

                # Store current values
                torque_demand.append(torque_step_current)
                demand_period.append(torque_demand_period_up)
                speed_demand.append(speed_step_current)
                speed_lim_fwd.append(speed_lim_fwd_current)
                speed_lim_rev.append(speed_lim_rev_current)
                test_point.append(log_up)

            while torque_step_current < torque_min:

                torque_step_current = torque_step_current - torque_step_down

                if torque_step_current >= torque_min:

                    torque_step_current = 0

                    # Store current values
                    torque_demand.append(torque_step_current)
                    demand_period.append(torque_demand_period_down)
                    speed_demand.append(speed_step_current)
                    speed_lim_fwd.append(speed_lim_fwd_current)
                    speed_lim_rev.append(speed_lim_rev_current)
                    test_point.append(log_down)
                    break

                # Store current values
                torque_demand.append(torque_step_current)
                demand_period.append(torque_demand_period_down)
                speed_demand.append(speed_step_current)
                speed_lim_fwd.append(speed_lim_fwd_current)
                speed_lim_rev.append(speed_lim_rev_current)
                test_point.append(log_down)

        speed_step_current = speed_step_current + speed_step * speed_direction

        if speed_lim_method == "Percentage":
            speed_lim_fwd_current = max(speed_step_current * speed_lim_fwd_threshold, 0)
            speed_lim_rev_current = min(speed_step_current * speed_lim_rev_threshold, 0)

    ref = pd.DataFrame(data={"torque_demand": torque_demand, "demand_period": demand_period, "speed_demand": speed_demand, "speed_lim_fwd": speed_lim_fwd, "speed_lim_rev": speed_lim_rev, "test_point": test_point})

    return ref


def envolope_ref(torque_dir, speed_dir, speed_breakpoints, voltage_ref, interp_function):
    '''
    generate envelope references
    '''

    torque_envelope = []
    speed_envelope = []

    speed_max = max(speed_breakpoints) * speed_dir
    envolope_speed = np.linspace(0, max(speed_breakpoints) * speed_dir, 50)

    speed_envelope.append(0)
    torque_envelope.append(0)

    torque_max_available = 0

    for speeds in envolope_speed:

        torque_max_available = interp_function(voltage_ref, speeds)
        torque_max_available = torque_max_available[0]

        torque_envelope.append(torque_max_available * torque_dir)
        speed_envelope.append(speeds)

    speed_envelope.append(speed_max)
    torque_envelope.append(0)

    speed_envelope.append(0)
    torque_envelope.append(0)

    env = pd.DataFrame(data={"torque_envelope": torque_envelope, "speed_envelope": speed_envelope})

    return env


def idq_ref(ref_dict):
    '''
    Returns a Dataframe of Iq, Id, Dyno Speed Demand,  MCU Speed Limit Forward,MCU Speed Limit Reverse.

        Parameters:
            idq_mode (string)       : The method to generate Idq Injection Values: [idq] - Generate a grid of Iq (+/-), Id (-) Values, whose combination shall not exceed that defined by the maximum_current, [angle] - Generate  Iq (+/-), Id (-) Values by creating a vector whos maginude and angle is sweeped,the magnitude will not exceed that defined by the maximum current
            maximum_current (float) : The maximum stator current defined by the lower of; Inverter Manufacturer, Motor Manufacturer.
            iq_dir (int)             : Interger to determine Iq current generation.
            speed_val (float)        : Speed point to generate at.
            speed_dir (int)          : Interger to determine speed related reference points.
            speed_lim_gain (float)    : Gain used to determine how far away the speed limit is to the speed demand.
            current_step (float)           : The step between the current Idq value and next during generation.
            demand_period (float)   : Time between the injection points

        Returns:
            ref (Dataframe)         :  Dataframe of Iq, Id, Dyno Speed Demand,  MCU Speed Limit Forward,MCU Speed Limit Reverse.
    '''

    # Initialise Variables
    iq_val = 0

    # Initilise Lists
    iq_inject = []
    id_inject = []
    is_inject = []
    speed_demand = []
    speed_lim_fwd = []
    speed_lim_rev = []
    time_period = []
    test_point = []

    log_up = True
    log_down = False
    
    iq_dir = ref_dict["Idq"]["Iq Direction"]
    maximum_current = ref_dict["Idq"]["Maximum"]
    speed_val = ref_dict["Speed"]["Target"]
    speed_dir = ref_dict["Speed"]["Direction"]
    speed_lim_gain = ref_dict["Speed"]["Limit"]
    time_val = ref_dict["Idq"]["Step Period"]
    current_step = ref_dict["Idq"]["Step"]

    step_down_method = ref_dict["Idq"]["Step Method"]
    step_up_method = ref_dict["Idq"]["Step Method"]
    ramp_period = ref_dict["Idq"]["Ramp Period"]

    # If the Iq direction is positive
    if iq_dir > 0:

        # While the Iq Value is less than the stator current
        while iq_val <= maximum_current * iq_dir:
            id_val = 0

            if step_up_method == "Ramp":
                iq_val_ramp = 0
                id_val_ramp = id_val

                if iq_val != 0:
                    while iq_val_ramp != iq_val - current_step:
                        iq_val_ramp = iq_val_ramp + current_step * iq_dir
                        iq_inject.append(iq_val_ramp)
                        id_inject.append(id_val_ramp)
                        is_inject.append(np.sqrt((id_val_ramp ** 2) + (iq_val_ramp ** 2)))
                        speed_demand.append(speed_val * speed_dir)
                        speed_lim_fwd.append(speed_val * (speed_lim_gain / 100))
                        speed_lim_rev.append(speed_val * (speed_lim_gain / 100))
                        time_period.append(ramp_period)
                        test_point.append(log_down)

            # While the Id Value is then the stator current (*-1)
            while id_val >= maximum_current * -1:

                # If the stator current for the combination of Idq Values is going to be greater than the Maximum stator current, stop.
                if np.sqrt((id_val ** 2) + (iq_val ** 2)) > maximum_current:

                    if step_down_method == "Ramp":
                        iq_val_ramp = iq_val
                        id_val_ramp = id_val
                        while np.sqrt((id_val_ramp ** 2) + (iq_val_ramp ** 2)) > 0:

                            if iq_val_ramp != 0:
                                iq_val_ramp = iq_val_ramp - (current_step * iq_dir)
                            if id_val_ramp != 0:
                                id_val_ramp = id_val_ramp + (current_step)

                            iq_inject.append(iq_val_ramp)
                            id_inject.append(id_val_ramp)
                            is_inject.append(np.sqrt((id_val_ramp ** 2) + (iq_val_ramp ** 2)))
                            speed_demand.append(speed_val * speed_dir)
                            speed_lim_fwd.append(speed_val * (speed_lim_gain / 100))
                            speed_lim_rev.append(speed_val * (speed_lim_gain / 100))
                            time_period.append(ramp_period)
                            test_point.append(log_down)

                    break

                # Otherwise add the point to a list
                else:
                    iq_inject.append(iq_val)
                    id_inject.append(id_val)
                    is_inject.append(np.sqrt((id_val ** 2) + (iq_val ** 2)))
                    speed_demand.append(speed_val * speed_dir)
                    speed_lim_fwd.append(speed_val * (speed_lim_gain / 100))
                    speed_lim_rev.append(speed_val * (speed_lim_gain / 100))
                    time_period.append(time_val)
                    test_point.append(log_up)

                    # Once the value is added to the list, increase by the requested current step
                    id_val = id_val - current_step
            iq_val = iq_val + current_step * iq_dir

    elif iq_dir < 0:
        # While the Iq Value is less than the stator current (*-1)
        while iq_val >= maximum_current * iq_dir:
            id_val = 0

            if step_up_method == "Ramp":
                iq_val_ramp = 0
                id_val_ramp = id_val

                if iq_val != 0:
                    while iq_val_ramp != iq_val + current_step:
                        iq_val_ramp = iq_val_ramp + current_step * iq_dir
                        iq_inject.append(iq_val_ramp)
                        id_inject.append(id_val_ramp)
                        is_inject.append(np.sqrt((id_val_ramp ** 2) + (iq_val_ramp ** 2)))
                        speed_demand.append(speed_val * speed_dir)
                        speed_lim_fwd.append(speed_val * (speed_lim_gain / 100))
                        speed_lim_rev.append(speed_val * (speed_lim_gain / 100))
                        time_period.append(ramp_period)
                        test_point.append(log_down)

            # While the Id Value is then the stator current (*-1)
            while id_val >= maximum_current * -1:

                # If the stator current for the combination of Idq Values is going to be greater than the Maximum stator current, stop.
                if np.sqrt((id_val ** 2) + (iq_val ** 2)) > maximum_current:

                    if step_down_method == "Ramp":
                        iq_val_ramp = iq_val
                        id_val_ramp = id_val
                        while np.sqrt((id_val_ramp ** 2) + (iq_val_ramp ** 2)) > 0:

                            if iq_val_ramp != 0:
                                iq_val_ramp = iq_val_ramp - (current_step * iq_dir)
                            if id_val_ramp != 0:
                                id_val_ramp = id_val_ramp + (current_step)

                            iq_inject.append(iq_val_ramp)
                            id_inject.append(id_val_ramp)
                            is_inject.append(np.sqrt((id_val_ramp ** 2) + (iq_val_ramp ** 2)))
                            speed_demand.append(speed_val * speed_dir)
                            speed_lim_fwd.append(speed_val * (speed_lim_gain / 100))
                            speed_lim_rev.append(speed_val * (speed_lim_gain / 100))
                            time_period.append(ramp_period)
                            test_point.append(log_down)

                    break

                # Otherwise add the point to a list
                else:
                    iq_inject.append(iq_val)
                    id_inject.append(id_val)
                    is_inject.append(np.sqrt((id_val ** 2) + (iq_val ** 2)))
                    speed_demand.append(speed_val * speed_dir)
                    speed_lim_fwd.append(speed_val * speed_dir * (speed_lim_gain / 100))
                    speed_lim_rev.append(speed_val * speed_dir * (speed_lim_gain / 100))
                    time_period.append(time_val)
                    test_point.append(log_up)

                    # Once the value is added to the list, increase by the requested current step
                    id_val = id_val - current_step
            iq_val = iq_val + current_step * iq_dir


    ref = pd.DataFrame(data={"iq_injected": iq_inject, "id_injected": id_inject, "is_injected": is_inject, "speed_demand": speed_demand, "speed_lim_fwd": speed_lim_fwd, "speed_lim_rev": speed_lim_fwd, "demand_period": time_period, "test_point": test_point})

    return ref

# TABLE GENERATOR FUNCTIONS


def torque_speed_table(gen_cfg):
    '''
    generate torque speed table
    '''

    torque_max_pc = gen_cfg["Torque"]["Maximum"] / 100
    speed_lim_val = gen_cfg["Speed"]["Limit Value"] / 100

    ref_dict = {
        "Torque": {
            "Maximum": torque_max_pc,
            "Minimum": gen_cfg["Torque"]["Minimum"],
            "Ramp Up Value": gen_cfg["Torque"]["Ramp Up Value"],
            "Ramp Down Value": gen_cfg["Torque"]["Ramp Down Value"],
            "Ramp Up Period": gen_cfg["Torque"]["Ramp Up Period"],
            "Ramp Down Period": gen_cfg["Torque"]["Ramp Down Period"],
            "Skip": gen_cfg["Torque"]["Skip"],
        },
        "Speed": {
            "Max Table": gen_cfg["Speed"]["Max Table"],
            "Breakpoints": gen_cfg["Speed"]["Breakpoints"],
            "Maximum": gen_cfg["Speed"]["Maximum"],
            "Minimum": gen_cfg["Speed"]["Minimum"],
            "Step": gen_cfg["Speed"]["Step"],
            "Limit Type": gen_cfg["Speed"]["Limit Type"],
            "Limit Value": speed_lim_val,
        },
        "Voltage": {
            "Voltage": gen_cfg["Voltage"]["Voltage"]
        },
        "Logging": {
            "Log Up": gen_cfg["Logging"]["Up"],
            "Log Down": gen_cfg["Logging"]["Down"]
        },
        "Interpolation": {
            "Function": None
        }

    }

    init_count = 0

    ref_init = pd.DataFrame(data={
        "torque_demand": [], 
        "demand_period": [], 
        "speed_demand": [], 
        "speed_demand_rads": [], 
        "speed_lim_fwd": [], 
        "speed_lim_rev": [], 
        "power_mech": []
        })

    xx, yy = np.meshgrid(gen_cfg["Voltage"]["Breakpoints"], gen_cfg["Speed"]["Breakpoints"])

    if gen_cfg["Test"]["Profile"] == "Forward Motoring":
        profile_order = ["Q1"]

    elif gen_cfg["Test"]["Profile"] == "Reverse Generating":
        profile_order = ["Q2"]

    elif gen_cfg["Test"]["Profile"] == "Reverse Motoring":
        profile_order = ["Q3"]

    elif gen_cfg["Test"]["Profile"] == "Forward Generating":
        profile_order = ["Q4"]

    elif gen_cfg["Test"]["Profile"] == "Motoring":
        profile_order = ["Q1", "Q3"]

    elif gen_cfg["Test"]["Profile"] == "Generating":
        profile_order = ["Q2", "Q4"]

    elif gen_cfg["Test"]["Profile"] == "Forward":
        profile_order = ["Q1", "Q4"]

    elif gen_cfg["Test"]["Profile"] == "Reverse":
        profile_order = ["Q2", "Q3"]

    elif gen_cfg["Test"]["Profile"] == "All":
        profile_order = ["Q1", "Q2", "Q3", "Q4"]

    for quadrant in profile_order:

        if quadrant == "Q1":

            ref_dict["Speed"]["Direction"] = 1
            ref_dict["Torque"]["Direction"] = 1

            ref_dict["Interpolation"]["Function"] = interp2d(
                x=xx,
                y=yy * ref_dict["Speed"]["Direction"],
                z=gen_cfg["Torque"]["Peak"],
                kind='linear',
                copy=True,
                bounds_error=False,
                fill_value=None
            )

            ref = torque_speed_ref(ref_dict)

        elif quadrant == "Q2":

            ref_dict["Speed"]["Direction"] = -1
            ref_dict["Torque"]["Direction"] = 1

            ref_dict["Interpolation"]["Function"] = interp2d(
                x=xx,
                y=yy * ref_dict["Speed"]["Direction"],
                z=gen_cfg["Torque"]["Peak"],
                kind='linear',
                copy=True,
                bounds_error=False,
                fill_value=None
            )

            ref = torque_speed_ref(ref_dict)

        elif quadrant == "Q3":

            ref_dict["Speed"]["Direction"] = -1
            ref_dict["Torque"]["Direction"] = -1

            ref_dict["Interpolation"]["Function"] = interp2d(
                x=xx,
                y=yy * ref_dict["Speed"]["Direction"],
                z=gen_cfg["Torque"]["Peak"],
                kind='linear',
                copy=True,
                bounds_error=False,
                fill_value=None
            )

            ref = torque_speed_ref(ref_dict)

        elif quadrant == "Q4":

            ref_dict["Speed"]["Direction"] = 1
            ref_dict["Torque"]["Direction"] = -1

            ref_dict["Interpolation"]["Function"] = interp2d(
                x=xx,
                y=yy * ref_dict["Speed"]["Direction"],
                z=gen_cfg["Torque"]["Peak"],
                kind='linear',
                copy=True,
                bounds_error=False,
                fill_value=None
            )

            ref = torque_speed_ref(ref_dict)

        if init_count == 0:

            ref_out = pd.concat([ref_init, ref], ignore_index=True)
            init_count = 1

        else:

            ref_out = pd.concat([ref_out, ref], ignore_index=True)

    return ref_out


def envelope_table(torque_peak, speed_breakpoints, voltage_breakpoints, voltage_ref):
    '''
    generate envelope table
    '''
    init_count = 0
    envInit = pd.DataFrame(data={"torque_envelope": [], "speed_envelope": []})

    xx, yy = np.meshgrid(voltage_breakpoints, speed_breakpoints)

    profile_order = ["Q1", "Q2", "Q3", "Q4"]

    for quadrant in profile_order:

        if quadrant == "Q1":

            speed_dir = 1
            torque_dir = 1
            interp_function = interp2d(
                x=xx,
                y=yy * speed_dir,
                z=torque_peak,
                kind='linear',
                copy=True,
                bounds_error=False,
                fill_value=None
            )

            env = envolope_ref(
                torque_dir=torque_dir,
                speed_dir=speed_dir,
                speed_breakpoints=speed_breakpoints,
                voltage_ref=voltage_ref,
                interp_function=interp_function
            )

        elif quadrant == "Q2":
            speed_dir = -1
            torque_dir = 1
            interp_function = interp2d(
                x=xx,
                y=yy * speed_dir,
                z=torque_peak,
                kind='linear',
                copy=True,
                bounds_error=False,
                fill_value=None
            )

            env = envolope_ref(
                torque_dir=torque_dir,
                speed_dir=speed_dir,
                speed_breakpoints=speed_breakpoints,
                voltage_ref=voltage_ref,
                interp_function=interp_function
            )

        elif quadrant == "Q3":
            speed_dir = -1
            torque_dir = -1
            interp_function = interp2d(
                x=xx,
                y=yy * speed_dir,
                z=torque_peak,
                kind='linear',
                copy=True,
                bounds_error=False,
                fill_value=None
            )

            env = envolope_ref(
                torque_dir=torque_dir,
                speed_dir=speed_dir,
                speed_breakpoints=speed_breakpoints,
                voltage_ref=voltage_ref,
                interp_function=interp_function
            )

        elif quadrant == "Q4":
            speed_dir = 1
            torque_dir = -1
            interp_function = interp2d(
                x=xx,
                y=yy * speed_dir,
                z=torque_peak,
                kind='linear',
                copy=True,
                bounds_error=False,
                fill_value=None
            )

            env = envolope_ref(
                torque_dir=torque_dir,
                speed_dir=speed_dir,
                speed_breakpoints=speed_breakpoints,
                voltage_ref=voltage_ref,
                interp_function=interp_function
            )

        if init_count == 0:
            env_out = pd.concat([envInit, env], ignore_index=True)
            init_count = 1

        else:
            env_out = pd.concat([env_out, env], ignore_index=True)

    return env_out


def idq_table(gen_cfg):
    '''
    generate idq table
    '''

    ref_dict = gen_cfg

    init_count = 0

    ref_init = pd.DataFrame(data={
        "iq_injected": [],
        "id_injected": [],
        "statorCurrent": [],
        "speed_demand": [],
        "speed_demand_rads": [],
        "speed_lim_fwd": [],
        "speed_lim_rev": [],
        "demand_period": [],
        "test_point": []
        })

    if ref_dict["Test"]["Profile"] == "Forward Motoring":
        profile_order = ["Q1"]

    elif ref_dict["Test"]["Profile"] == "Reverse Generating":
        profile_order = ["Q2"]

    elif ref_dict["Test"]["Profile"] == "Reverse Motoring":
        profile_order = ["Q3"]

    elif ref_dict["Test"]["Profile"] == "Forward Generating":
        profile_order = ["Q4"]

    elif ref_dict["Test"]["Profile"] == "Motoring":
        profile_order = ["Q1", "Q3"]

    elif ref_dict["Test"]["Profile"] == "Generating":
        profile_order = ["Q2", "Q4"]

    elif ref_dict["Test"]["Profile"] == "Forward":
        profile_order = ["Q1", "Q4"]

    elif ref_dict["Test"]["Profile"] == "Reverse":
        profile_order = ["Q2", "Q3"]

    elif ref_dict["Test"]["Profile"] == "All":
        profile_order = ["Q1", "Q2", "Q3", "Q4"]

    for quadrant in profile_order:

        if quadrant == "Q1":
            
            ref_dict["Speed"]["Direction"] = 1
            ref_dict["Idq"]["Iq Direction"] = 1

            ref = idq_ref(ref_dict)

        elif quadrant == "Q2":
            ref_dict["Speed"]["Direction"] = -1
            ref_dict["Idq"]["Iq Direction"] = 1
            ref = idq_ref(ref_dict)

        elif quadrant == "Q3":
            ref_dict["Speed"]["Direction"] = -1
            ref_dict["Idq"]["Iq Direction"] = -1
            ref = idq_ref(ref_dict)

        elif quadrant == "Q4":
            ref_dict["Speed"]["Direction"] = 1
            ref_dict["Idq"]["Iq Direction"] = -1
            ref = idq_ref(ref_dict)

        if init_count == 0:
            ref_out = pd.concat([ref_init, ref], ignore_index=True)
            init_count = 1

        else:
            ref_out = pd.concat([ref_out, ref], ignore_index=True)

    ref_out["statorCurrent"] = np.sqrt((ref_out["iq_injected"] * ref_out["iq_injected"]) + (ref_out["id_injected"] * ref_out["id_injected"]))
    ref_out["speed_demand_rads"] = (ref_out["speed_demand"] / 60) * (2 * np.pi)

    return ref_out

# SCRIPT GENERATOR FUNCTIONS


def torque_speed_script(input):
    '''
    Generate the top of the test script for torque speed sweep
    '''
    script = []
    script.append("import os")
    script.append("import sys")
    script.append("import time")
    script.append("import ctypes")
    script.append("from enum import Enum")
    script.append("import numpy as np")
    script.append("import pandas as pd")
    script.append("from loguru import logger as lg")
    script.append("import MunchPy as mpy")
    script.append("import program.src.tenma as tenma") 
    script.append("import program.src.torque_sense as ts")
    script.append("")
    script.append("#------------------ INPUT DATA ------------------")
    script.append("")
    script.append("###### Test Setup ")
    script.append("# Name: " + str(input["Test"]["Name"]))
    script.append("# Log Directory: " + str(input["Test"]["Logging Path"]))
    script.append("")
    script.append("###### Inverter ")
    script.append("# Name: " + str(input["MCU"]["Name"]))
    script.append("# Sample Letter: " + str(input["MCU"]["Sample Letter"]))
    script.append("# Sample Number: " + str(input["MCU"]["Sample Number"]))
    script.append("# Notes: " + str(input["MCU"]["Note"]))
    script.append("")
    script.append("###### Motor ")
    script.append("# Manufacturer: " + str(input["Motor"]["Manufacturer"]))
    script.append("# Sample Letter: " + str(input["Motor"]["Sample Letter"]))
    script.append("# Sample Number: " + str(input["Motor"]["Sample Number"]))
    script.append("# Notes: " + str(input["Motor"]["Note"]))
    script.append("")
    script.append("###### Dynamometer ")
    script.append("# Location: " + str(input["Dyno"]["Location"]))
    script.append("# IP Address: " + str(input["Dyno"]["IP"]))
    script.append("# Port: " + str(input["Dyno"]["Port"]))
    script.append("# ID: " + str(input["Dyno"]["ID"]))
    script.append("")
    script.append("##### CAN Hardware ")
    script.append("# DCDC Ixxat ID: " + str(input["CAN"]["DCDC CAN HWID"]))
    script.append("# MCU Ixxat ID: " +  str(input["CAN"]["MCU CAN HWID"]))
    script.append("")
    script.append("##### Test ")
    script.append("# Test Type: " + str(input["Test"]["Type"]))
    script.append("# Test Profile: " + str(input["Test"]["Profile"]))
    script.append("")
    script.append("###### DCDC ")
    script.append("# Target DC Link Voltage: " +  str(input["DCDC"]["Voltage"]))
    script.append("# DC Link Current Limit +: " + str(input["DCDC"]["Current +"]))
    script.append("# DC Link Current Limit -: " + str(input["DCDC"]["Current -"]))
    script.append("")
    # script.append("# Test Dwell ")
    # script.append("# Motor Temperature Test Upper Limit     = " + str(input_struct.motor_temp_upper))
    # script.append("# Motor Temperature Test Lower Limit     = " + str(input_struct.motor_temp_lower))
    # script.append("# Motor Temperature Test Ramp Step  = " + str(input_struct.motor_temp_step))
    script.append("")
    script.append("#================== START OF TEST SCRIPT ==================")
    script.append("")
    script.append("#------------------ TEST POINTS ------------------")
    script.append("")
    script.append("SPEED_DEMANDS         = " + str(input["Test Input"]["Speed Demands"]))
    script.append("TORQUE_DEMANDS        = " + str(input["Test Input"]["Torque Demands"]))
    script.append("SPEED_LIMITS_FORWARD  = " + str(input["Test Input"]["Speed Limits Forward"]))
    script.append("SPEED_LIMITS_REVERSE  = " + str(input["Test Input"]["Speed Limits Reverse"]))
    script.append("DEMAND_TIME           = " + str(input["Test Input"]["Demand Period"]))
    script.append("LOG_POINT             = " + str(input["Test Input"]["Logged Points"]))
    script.append("")
    script.append("DCDC_I_LIMIT_POS      = " + str(input["DCDC"]["Current +"]))
    script.append("DCDC_I_LIMIT_NEG      = " + str(input["DCDC"]["Current -"]))
    script.append("DCDC_V_TARGET         = " + str(input["DCDC"]["Voltage"]))
    script.append("")
    script.append("MCU_DISCHARGE_I_LIMIT = " + str(input["MCU"]["Discharge Limit"]))
    script.append("MCU_CHARGE_I_LIMIT    = " + str(input["MCU"]["Charge Limit"]))
    script.append("")
    script.append("#------------------ INITIALISATION ------------------")
    script.append("")
    script.append("LOGGING_PATH                = " + "r\"" + str(input["Test"]["Logging Path"]) + r"/MunchPy" + "\"")
    script.append("TEST_NAME                   = " + "\"" + str(input["Test"]["Name"]) + "\"")
    script.append("CAN_HWID_DCDC               = " + "\"" + str(input["CAN"]["DCDC CAN HWID"]) + "\"")
    script.append("CAN_HWID_MCU                = " + "\"" + str(input["CAN"]["MCU CAN HWID"]) + "\"")
    script.append("DYNO_LOCATION               = " + "\"" + str(input["Dyno"]["Location"]) + "\"")
    script.append("DYNO_IP                     = " + "\"" + str(input["Dyno"]["IP"]) + "\"")
    script.append("DYNO_PORT                   = " + str(input["Dyno"]["Port"]))
    script.append("DYNO_ID                     = " + str(input["Dyno"]["ID"]))
    script.append("GEARBOX_RATIO               = " + str(input["Dyno"]["Gearbox Ratio"]))
    if input["Login"]["Login"] is True:
        script.append("PASSWORD                    = " + "\"" + str(input["Login"]["Password"]) + "\"")
    script.append("TORQUE_SENSOR               = " + "\"" + str(input["Dyno"]["Transducer"]) + "\"")
    script.append("LOOP_SAMPLE_TIME            = " + str(input["Logging"]["Sample Period"]))
    script.append("LOG_DELAY_PERIOD            = " + str(input["Logging"]["Delay Period"]))
    if input["Dyno"]["Transducer"] == "TS":
        script.append("TORQUE_SENSOR_TORQUE_FILTER = " + str(input["Dyno"]["Transducer Torque Filter"]))
    script.append("LOGIN_USER                  = " + str(input["Login"]["Login"]))
    script.append("DELAY_LOGGING               = " + str(input["Logging"]["Delay"]))
    script.append("")
    #script.append("MOTOR_DWELL_ENABLE   = " + str(input_struct.motor_dwell_enable))
    #script.append("MOTOR_TEMP_UPPER     = " + str(input_struct.motor_temp_upper))
    #script.append("MOTOR_TEMP_LOWER     = " + str(input_struct.motor_temp_lower))
    #script.append("MOTOR_TEMP_IDQ_STEP  = " + str(input_struct.motor_temp_step))

    file_name = (input["Test"]["Export Path"] + input["Test"]["Export Name"] + input["Test"]["Export Format"])
    if os.path.exists(file_name):
        interp_function = open(file_name, 'w')
        interp_function.writelines("%s\n" % i for i in script)
        interp_function.close()
    else:
        interp_function = open(file_name, 'w')
        interp_function.writelines("%s\n" % i for i in script)
        interp_function.close()
    path = file_name

    return path


def idq_script(input):
    '''
    Generate the top of the test script for torque speed sweep
    '''
    script = []
    script.append("import os")
    script.append("import sys")
    script.append("import time")
    script.append("import ctypes")
    script.append("from enum import Enum")
    script.append("import numpy as np")
    script.append("import pandas as pd")
    script.append("from loguru import logger as lg")
    script.append("import MunchPy as mpy")
    script.append("import program.src.tenma as tenma") 
    script.append("import program.src.torque_sense as ts")
    script.append("")
    script.append("#------------------ INPUT DATA ------------------")
    script.append("")
    script.append("###### Test Setup ")
    script.append("# Name: " + str(input["Test"]["Name"]))
    script.append("# Log Directory: " + str(input["Test"]["Logging Path"]))
    script.append("")
    script.append("###### Inverter ")
    script.append("# Name: " + str(input["MCU"]["Name"]))
    script.append("# Sample Letter: " + str(input["MCU"]["Sample Letter"]))
    script.append("# Sample Number: " + str(input["MCU"]["Sample Number"]))
    script.append("# Notes: " + str(input["MCU"]["Note"]))
    script.append("")
    script.append("###### Motor ")
    script.append("# Manufacturer: " + str(input["Motor"]["Manufacturer"]))
    script.append("# Sample Letter: " + str(input["Motor"]["Sample Letter"]))
    script.append("# Sample Number: " + str(input["Motor"]["Sample Number"]))
    script.append("# Notes: " + str(input["Motor"]["Note"]))
    script.append("")
    script.append("###### Dynamometer ")
    script.append("# Location: " + str(input["Dyno"]["Location"]))
    script.append("# IP Address: " + str(input["Dyno"]["IP"]))
    script.append("# Port: " + str(input["Dyno"]["Port"]))
    script.append("# ID: " + str(input["Dyno"]["ID"]))
    script.append("")
    script.append("##### CAN Hardware ")
    script.append("# DCDC Ixxat ID: " + str(input["CAN"]["DCDC CAN HWID"]))
    script.append("# MCU Ixxat ID: " +  str(input["CAN"]["MCU CAN HWID"]))
    script.append("")
    script.append("##### Test ")
    script.append("# Test Type: " + str(input["Test"]["Type"]))
    script.append("# Test Profile: " + str(input["Test"]["Profile"]))
    script.append("")
    script.append("###### DCDC ")
    script.append("# Target DC Link Voltage: " +  str(input["DCDC"]["Voltage"]))
    script.append("# DC Link Current Limit +: " + str(input["DCDC"]["Current +"]))
    script.append("# DC Link Current Limit -: " + str(input["DCDC"]["Current -"]))
    script.append("")
    script.append("###### Speed ")
    script.append("# Test Speed Point: " + str(input["Speed"]["Target"]))
    script.append("# Value of Speed Limit: " + str(input["Speed"]["Limit Value"]))
    script.append("")
    if input["Temp"]["Enable"] is True:
        script.append("###### Test Dwell ")
        script.append("# Motor Temperature Test Upper Limit     = " + str(input["Temp"]["Limit Upper"]))
        script.append("# Motor Temperature Test Lower Limit     = " + str(input["Temp"]["Limit Lower"]))
        script.append("# Motor Temperature Test Ramp Step  = " + str(input["Temp"]["Step"]))
        script.append("")
    script.append("#****** START OF TEST SCRIPT ******")
    script.append("")
    script.append("#****** TEST POINTS ******")
    script.append("")
    script.append("SPEED_DEMANDS        = " + str(input["Test Input"]["Speed Demands"]))
    script.append("IQ_DEMANDS           = " + str(input["Test Input"]["Iq Demands"]))
    script.append("ID_DEMANDS           = " + str(input["Test Input"]["Id Demands"]))
    script.append("SPEED_LIMITS_FORWARD = " + str(input["Test Input"]["Speed Limits Forward"]))
    script.append("SPEED_LIMITS_REVERSE = " + str(input["Test Input"]["Speed Limits Reverse"]))
    script.append("DEMAND_TIME          = " + str(input["Test Input"]["Demand Period"]))
    script.append("LOG_POINT            = " + str(input["Test Input"]["Logged Points"]))
    script.append("")
    script.append("DCDC_I_LIMIT_POS      = " + str(input["DCDC"]["Current +"]))
    script.append("DCDC_I_LIMIT_NEG      = " + str(input["DCDC"]["Current -"]))
    script.append("DCDC_V_TARGET         = " + str(input["DCDC"]["Voltage"]))
    script.append("")
    script.append("MCU_DISCHARGE_I_LIMIT = " + str(input["MCU"]["Discharge Limit"]))
    script.append("MCU_CHARGE_I_LIMIT    = " + str(input["MCU"]["Charge Limit"]))
    script.append("")
    script.append("#****** INITIALISATION ******")
    script.append("#------------------ INITIALISATION ------------------")
    script.append("")
    script.append("LOGGING_PATH                = " + "r\"" + str(input["Test"]["Logging Path"]) + r"/MunchPy" + "\"")
    script.append("TEST_NAME                   = " + "\"" + str(input["Test"]["Name"]) + "\"")
    script.append("CAN_HWID_DCDC               = " + "\"" + str(input["CAN"]["DCDC CAN HWID"]) + "\"")
    script.append("CAN_HWID_MCU                = " + "\"" + str(input["CAN"]["MCU CAN HWID"]) + "\"")
    script.append("DYNO_LOCATION               = " + "\"" + str(input["Dyno"]["Location"]) + "\"")
    script.append("DYNO_IP                     = " + "\"" + str(input["Dyno"]["IP"]) + "\"")
    script.append("DYNO_PORT                   = " + str(input["Dyno"]["Port"]))
    script.append("DYNO_ID                     = " + str(input["Dyno"]["ID"]))
    script.append("GEARBOX_RATIO               = " + str(input["Dyno"]["Gearbox Ratio"]))
    if input["Login"]["Login"] is True:
        script.append("PASSWORD                    = " + "\"" + str(input["Login"]["Password"]) + "\"")
    script.append("TORQUE_SENSOR               = " + "\"" + str(input["Dyno"]["Transducer"]) + "\"")
    script.append("LOOP_SAMPLE_TIME            = " + str(input["Logging"]["Sample Period"]))
    script.append("LOG_DELAY_PERIOD            = " + str(input["Logging"]["Delay Period"]))
    if input["Dyno"]["Transducer"] == "TS":
        script.append("TORQUE_SENSOR_TORQUE_FILTER = " + str(input["Dyno"]["Transducer Torque Filter"]))
    script.append("LOGIN_USER                  = " + str(input["Login"]["Login"]))
    script.append("DELAY_LOGGING               = " + str(input["Logging"]["Delay"]))
    script.append("")
    script.append("TEST_MODE_ID         = " + str(input["Idq"]["Mode ID"]))
    script.append("TMODE_MODE_VAL       = 1.0")
    script.append("TMODE_AXIS_ID        = " + str(input["Idq"]["Axis ID"]))
    script.append("TMODE_AXIS_VAL       = " + str(input["Idq"]["Axis Value"]))
    script.append("ID_INJ_ID            = " + str(input["Idq"]["Id ID"]))
    script.append("IQ_INJ_ID            = " + str(input["Idq"]["Iq ID"]))
    script.append("")
    script.append("")
    if input["Temp"]["Enable"] is True:
        script.append("MOTOR_DWELL_ENABLE   = " + str(input["Temp"]["Enable"]))
        script.append("MOTOR_TEMP_UPPER     = " + str(input["Temp"]["Limit Upper"]))
        script.append("MOTOR_TEMP_LOWER     = " + str(input["Temp"]["Limit Lower"]))
        script.append("MOTOR_TEMP_IDQ_STEP  = " + str(input["Temp"]["Step"]))
    script.append("")

    file_name = (input["Test"]["Export Path"] + input["Test"]["Export Name"] + input["Test"]["Export Format"])
    if os.path.exists(file_name):
        interp_function = open(file_name, 'w')
        interp_function.writelines("%s\n" % i for i in script)
        interp_function.close()
    else:
        interp_function = open(file_name, 'w')
        interp_function.writelines("%s\n" % i for i in script)
        interp_function.close()
    path = file_name

    return path


def file_merge(first_file, second_file):

    f1 = open(first_file, "a+")
    f1.seek(0)
    f2 = open(second_file, 'r')

    f1.write(f2.read())

    f1.close()
    f2.close()
