from re import template
import numpy as np
import plotly.graph_objects as go
from scipy.interpolate import griddata
template_white = "plotly_white"

def torque_speed_scatter(profile, profile_voltage, project_speeds, project_torques):
    '''
    For the project profile, votlage, speed and speed limits return plot objects of the test points, max_imum available envolope and test timeline
    '''
    scatter = go.Figure()

    scatter.add_trace(go.Scattergl(
        x=project_speeds,
        y=project_torques,
        name="Avaiable Envelope",
        mode='lines',
    )
    )

    scatter.add_trace(go.Scattergl(
        x=profile.speed_demand,
        y=profile.torque_demand,
        name="Test Points",
        mode='markers',
    )
    )

    scatter.update_layout(
        template = template_white,
        title="Test Points: " + str(profile_voltage) + "V, " + str(min(profile.speed_demand)) + "rpm to " + str(max(profile.speed_demand)) + " rpm, ",
        xaxis_title="Speed [rpm]",
        yaxis_title="Torque Demanded [Nm]"
    )

    return scatter

def torque_speed_timeline(profile, profile_voltage, speed_limit_forward, speed_limit_reverse):
    # TIMELINe
    timeline = go.Figure()

    timeline.add_trace(go.Scattergl(
        y=profile.speed_demand,
        name="Speed (rpm)",
        yaxis='y1'
    ))

    timeline.add_trace(go.Scattergl(
        y=speed_limit_forward,
        name="Speed Limit Forward (rpm)",
        yaxis='y1'
    ))

    timeline.add_trace(go.Scattergl(
        y=speed_limit_reverse,
        name="Speed Limit Reverse (rpm)",
        yaxis='y1'
    ))

    timeline.add_trace(go.Scattergl(
        y=profile.torque_demand,
        name="Torque (Nm)",
        yaxis='y3'
    ))

    timeline.add_trace(go.Scattergl(
        y=profile.power_mech,
        name="Mech Power (W)",
        yaxis='y4'
    ))

    timeline.update_traces(mode="markers+lines")

    timeline.update_layout(
        template = template_white,
        xaxis=dict(
            domain=[0, 0.8]
        ),
        yaxis=dict(
            title="Speed [rpm]",
        ),
        yaxis3=dict(
            title="Torque [Nm]",
            anchor="x",
            overlaying="y",
            side="right"
        ),
        yaxis4=dict(
            title="Mech Power [W]",
            anchor="free",
            overlaying="y",
            side="right",
            position=0.9
        ),
        title="Test Input Timeline: " + str(profile_voltage) + "V, " + str(min(profile.speed_demand)) + "rpm to " + str(max(profile.speed_demand)) + " rpm, ",
        xaxis_title="Test Point",
        hovermode="x unified",
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=1.0,
            xanchor="right",
            x=1
        )
    )

    return timeline

def idq_scatter(profile, voltage_ref):
    '''
    For the input profile create a scatter plot object with each test point.
    '''

    if len(profile["speed_demand"].unique()) == 1:

        scatter = go.Figure()

        scatter.add_trace(go.Scattergl(
            x=profile.id_injected,
            y=profile.iq_injected,
            name="Test Points",
            mode='markers',
            marker=dict(
                color=profile.statorCurrent,
                colorscale='Bluered',
                colorbar=dict(title="Stator Current [Arms]", thickness=20, tick0=0, dtick=50),
            )
        )
        )

        scatter.update_layout(
            template = template_white,
            title="Idq Test Points: " + str(voltage_ref) + "V, " + str(min(profile.speed_demand)) + "rpm",
            xaxis_title="Id Current Injected [Arms]",
            yaxis_title="Iq Current Injected [Arms]"
        )

    else:
        scatter = go.Figure()

        scatter.add_trace(go.Scatter3d(
            x=profile.id_injected,
            y=profile.iq_injected,
            z=profile.speed_demand,
            name="Test Points",
            mode='markers',
            hovertemplate="Id (Arms)" + ': %{x:.2f}' +
            '<br>' + "Iq (Arms)" + ': %{y:.2f}</br>' +
            "Speed (rpm)" + ': %{z:.2f}',
            marker=dict(
                size=5,
                color=profile.statorCurrent,
                colorscale='Bluered',
                colorbar=dict(title="Stator Current [Arms]", thickness=20, tick0=0, dtick=50),
                opacity=1.0
            )
        )
        )

        scatter.update_layout(
            scene=dict(
                xaxis_title='Id [Arms]',
                yaxis_title='Iq [Arms]',
                zaxis_title='Speed (rpm)',
            )
        )
    return scatter

def idq_timeline(profile):
    # TIMELINE
    line = go.Figure()

    line.add_trace(go.Scattergl(
        y=profile.id_injected,
        name="Id Injected Current (Arms)",
        yaxis='y1'
    )
    )

    line.add_trace(go.Scattergl(
        y=profile.iq_injected,
        name="Iq Injected Current (Arms)",
        yaxis='y1'
    )
    )

    line.add_trace(go.Scattergl(
        y=np.sqrt((profile.id_injected * profile.id_injected) + (profile.iq_injected * profile.iq_injected)),
        name="Stator Current Injected (Arms)",
        yaxis='y1'
    )
    )

    line.add_trace(go.Scattergl(
        y=profile.speed_demand,
        name="Speed (Rpm)",
        yaxis='y2'
    )
    )

    line.update_traces(mode="markers+lines")

    line.update_layout(
        template =template_white,
        xaxis=dict(
            domain=[0, 0.8]
        ),
        yaxis=dict(
            title="Current [Arms]",
        ),
        yaxis2=dict(
            title="Speed [Rpm]",
            anchor="x",
            overlaying="y",
            side="right"
        ),
        title="Test Input Timeline: " + "V, " + str(min(profile.speed_demand)) + "rpm to " + str(max(profile.speed_demand)) + " rpm, ",
        xaxis_title="Test Point",
        hovermode="x unified",
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=1.0,
            xanchor="right",
            x=1
        )
    )

    return line


def surface_idq(df_all, df_45, df_0):
    '''
    For dataframe of all , 45 degrees and 0 degress return surface plot object
    '''
    x = df_all["Id Current Injected"]
    y = df_all["Iq Current Injected"]
    z = df_all["Torque Measured [Nm]"]

    x_i = np.linspace(start=float(min(x)), stop=float(max(x)), num=int(50))
    y_i = np.linspace(start=float(min(y)), stop=float(max(y)), num=int(50))

    X, Y = np.meshgrid(x_i, y_i)

    z_out = griddata((x, y), z, (X, Y), method='linear')

    x_out = x_i
    y_out = y_i

    plot = go.Figure()

    plot.add_trace(go.Surface(
        z=z_out,
        x=x_out,
        y=y_out,

        contours={
            "x": {"show": True},
            "z": {"show": True}
        },
        opacity=0.98,
        colorscale="Jet",

        hovertemplate="Id Injected" + ': %{x:.2f} A' +
        '<br>' + "Iq Injected" + ': %{y:.2f} A</br>' +
        "Torque Measured" + ': %{z:.2f} Nm',

        colorbar=dict(
            title="Torque Measured",
            titleside='right',
            titlefont=dict(
                size=12,
                family='Arial, sans'
            ),)
    ))

    plot.add_trace(go.Scatter3d(
        x=df_0["Id Current Injected"],
        y=df_0["Iq Current Injected"],
        z=df_0["Torque Measured [Nm]"],
        name="Injected 0 θ",
        mode='markers',
        marker=dict(
            size=5,
            color='magenta',
            symbol='x'
        )
    ))

    plot.add_trace(go.Scatter3d(
        x=df_45["Id Current Injected"],
        y=df_45["Iq Current Injected"],
        z=df_45["Torque Measured [Nm]"],
        name="Injected 45 θ",
        mode='markers',
        marker=dict(
            size=5,
            color='black',
            symbol='x'
        )
    ))

    plot.update_traces(mode='markers', selector=dict(type='scatter3d'))

    plot.update_layout(
        template = template_white,
        title="Idq Injected and Resulting Torque Measured",
        scene=dict(
            xaxis_title='Id Current Injected',
            yaxis_title='Iq Curent Injected',
            zaxis_title='Torque Measured [Nm]'
        ),
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=1.02,
            xanchor="right",
            x=1
        )
    )

    return plot


def psi_m(df_0, interp_vals):
    '''
    for dataframe (is angle at 0 degrees), produce a scatter and line plot with the interpoloated valus
    '''
    plot = go.Figure()

    plot.add_trace(go.Scatter(
        x=np.linspace(min(df_0["Iq Current Injected"]), max(df_0["Iq Current Injected"]), num=50),
        y=interp_vals,
        name="Psi m Interpolated"
    ))

    plot.add_trace(go.Scatter(
        x=df_0["Iq Current Injected"],
        y=df_0["psi m"],
        name="Psi m",
        mode="markers",
        marker_size=10
    ))

    plot.update_layout(
        template = template_white,
        title="Psi m interpolated Values against Iq Current",
        xaxis_title="Iq Injected Current [0 Degrees]",
        yaxis_title="Psi m"
    )

    return plot


def ld_minus_lq(df_45):
    '''
    produce the ld - lq plot us the dataframe (with IS degrees at 45)
    '''
    plot = go.Figure()

    plot.add_trace(go.Scatter(
        x=df_45['Is Current Injected'],
        y=df_45['Ld - Lq']
    ))

    plot.update_layout(
        template = template_white,
        title="Ld - Lq",
        xaxis_title='Is Current Injected',
        yaxis_title="Ld - Lq"
    )

    return plot


def speed_torque_error_contour(df_all, error_type):
    '''
    plot torque error
    '''
    x_in = df_all["Speed Demanded [rpm]"]
    y_in = df_all["Torque Demanded Actual"]
    z_in = df_all["Torque Demanded Actual Error " + str(error_type)]

    x_i = np.linspace(
        start=float(min(x_in)),
        stop=float(max(x_in)),
        num=int(50)
    )
    
    y_i = np.linspace(
        start=float(min(y_in)),
        stop=float(max(y_in)),
        num=int(50)
    )

    X, Y = np.meshgrid(
        x_i, 
        y_i
    )

    z_out = griddata(
        points=(x_in, y_in),
        values=z_in,
        xi=(X, Y),
        method='linear'
    )

    x_out = x_i
    y_out = y_i

    plot = go.Figure()

    plot.add_trace(go.Contour(
        z=z_out,
        x=x_out,
        y=y_out,
        name="Contour",
        opacity=0.98,
        colorscale="Jet",

        hovertemplate="Speed Demanded" + ': %{x:.2f} rpm' +
        '<br>' + "Torque Demanded Actual" + ': %{y:.2f} Nm</br>' +
        "Torque Measured Error" + ': %{z:.2f} ' + str(error_type),

        colorbar=dict(
            title="Torque Error " + str(error_type),
            titleside='right',
            titlefont=dict(
                size=12,
                family='Arial, sans'
            ),
        ),
    ))

    plot.update_traces(
        zmid=0,
        showlegend=True,
        ncontours=20,
        contours=dict(
            coloring='heatmap',
            showlabels=True,

        )
    )

    plot.add_trace(go.Scatter(
        x=x_in,
        y=y_in,
        mode='markers',
        name='Logged Point',
        marker_symbol='cross',
        marker_color='black',
        hovertemplate="Speed Demanded" + ': %{x:.2f} rpm' +
        '<br>' + "Torque Demanded Actual" + ': %{y:.2f} Nm</br>'
    ))

    plot.update_layout(
        template = template_white,
        title="Torque Demanded Actual Error " + str(error_type),
        xaxis_title='Speed [rpm]',
        yaxis_title='Torque Demanded Actual [Nm]',
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=1.02,
            xanchor="right",
            x=1
        )
    )

    return plot


def speed_torque_error_line(df_all, error_type):
    '''
    for input dataframe create line plot of torque error and relevent data
    '''
    x_0 = df_all["Test Point"]
    y_0 = df_all["Torque Demanded"]
    y_1 = df_all["Torque Demanded Actual"]
    y_2 = df_all["Torque Measured [Nm]"]
    y_3 = df_all["Torque Demanded Actual Error " + str(error_type)]
    y_4 = df_all["Speed Demanded [rpm]"]

    plot = go.Figure()

    plot.add_trace(go.Scatter(
        x=x_0,
        y=y_0,
        name="Torque Demanded [Nm]",
        yaxis="y1",
    )
    )

    plot.add_trace(go.Scatter(
        x=x_0,
        y=y_1,
        name="Torque Demanded Actual [Nm]",
        yaxis="y1",
    )
    )

    plot.add_trace(go.Scatter(
        x=x_0,
        y=y_2,
        name="Torque Measured [Nm]",
        yaxis="y1",
    )
    )

    plot.add_trace(go.Scatter(
        x=x_0,
        y=y_3,
        yaxis="y3",
        name="Torque Demanded Actual Error " + error_type,
    )
    )

    plot.add_trace(go.Scatter(
        x=x_0,
        y=y_4,
        yaxis="y4",
        name="Speed Demanded [rpm]",
    )
    )

    plot.update_layout(
        hovermode='x unified',
    )

    plot.update_traces(mode="markers+lines")

    plot.update_layout(
        template = template_white,
        xaxis=dict(
            domain=[0, 0.8]
        ),
        yaxis=dict(
            title="Torque [Nm]",
        ),
        yaxis3=dict(
            title="Torque Error " + str(error_type),
            anchor="x",
            overlaying="y",
            side="right"
        ),
        yaxis4=dict(
            title="Speed [rpm]",
            anchor="free",
            overlaying="y",
            side="right",
            position=0.9
        ),
        title="Torque Actual Error " + str(error_type),
        xaxis_title="Test Point",
        hovermode="x unified",
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=1.0,
            xanchor="right",
            x=1
        )
    )

    return plot

def current_sensor_latency(data):
    plot = go.Figure()

    plot.add_trace(go.Scattergl(
        x = data["Speed"],
        y=data["Trim Angle"],
        name="Current Sensor Latency"
    ))

    plot.update_traces(mode="markers+lines")

    plot.update_layout(
        template = template_white,
        title="Current Sensor Latency",
        xaxis_title="Frequency",
        yaxis_title="Trim Angle"
    )

    return plot