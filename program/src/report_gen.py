from asyncio import constants
import os
from os.path import exists
import zipfile
import shutil
import pandas as pd
import datapane as dp
import numpy as np
from scipy import interpolate
from src import plot
from src import constants
import streamlit as st

def report_input(paths, hw_dict, test_dict, cfg_dict, plot_dict, test_points, test_name):
    '''
    s
    '''

    i_sense_dict = {
        "Speed": "Not Read",
        "Trim Angle": "Not Read"
    }

    inverter_dict = {
            "Ke" : "Not Read" ,
            "Offset" : "Not Read",
            "Pole Pairs": "Not Read",
            "Build Time" : "Not Read",
            "Build Name" : "Not Read",
            "IOP CRC" : "Not Read",
            "MCP CRC" : "Not Read",
            "MCP Serial" : "Not Read",
            "IOP Serial" : "Not Read"
        }

    measured_dict = None

    soft_data = read_inverter_properties(path= paths["inverter"], dict = inverter_dict)
    i_sense_data = csv_to_dict(path=paths["current"], dict = i_sense_dict )
    results = csv_to_dict(path=paths["measured"], dict = measured_dict )

    report_dict = {
        "Config": {
            "Name": test_name,
            "MCU": hw_dict["MCU"],
            "Motor": hw_dict["Motor"],
            "Dyno": hw_dict["Dyno"],
            "DCDC": hw_dict["DCDC"],
            "CAN" : hw_dict["CAN"],
            "Temp": cfg_dict["Temp"],
            "Logging": cfg_dict["Logging"],
            "Test": test_dict
        },
        "Paths": paths,
        "Data": {
            "Measured": results,
            "Input": test_points,
            "Current Sensor": i_sense_data,
            "Inverter Properties": soft_data,
            },
        "Plots": {
            "Input": {
                "Scatter":plot_dict["Scatter"],
                "Line":plot_dict["Line"]
            }
        }
    }

    return report_dict


def generate(paths, hw_dict, test_dict, cfg_dict, plot_dict, test_points, test_name):
    '''
    test_name, paths, config_table, soft_table, i_sense_table, input_data, input_plots
    '''
    report_dict = report_input(paths, hw_dict, test_dict, cfg_dict, plot_dict, test_points, test_name)

    if report_dict["Config"]["Test"]["Type"] == "Torque Speed Sweep":

        report_dict["Paths"]["base"] = "REPORT_TRQ_SPD_SWP_" + report_dict["Config"]["Name"]
        report_dict["Paths"]["report"] = report_dict["Paths"]["base"] + ".html"

        report_dict = torque_speed_sweep_plots(report_dict)
        report = torque_speed_sweep_report(report_dict)
        
    if report_dict["Config"]["Test"]["Type"] == "Idq Characterisation":

        report_dict["Paths"]["base"] = "REPORT_IDQ_CHAR_"+ report_dict["Config"]["Name"]
        report_dict["Paths"]["report"] = report_dict["Paths"]["base"] + ".html"
        
        report_dict = idq_data_clean(report_dict)
        report_dict = idq_characterisation_plots(report_dict)
        report = idq_characterisation_report(report_dict)
        
    report.save(
        path=report_dict["Paths"]["report"],
        formatting=constants.REPORT_FORMAT
    )

    # Compress and delete HTML
    report_dict["Paths"]["zip"] = report_dict["Paths"]["base"] + ".zip"
    zipped, report_message = zip_report( report_path=report_dict["Paths"]["report"], zip_path=report_dict["Paths"]["zip"]) 

    if zipped is True:
        delete_report(
            report_path=report_dict["Paths"]["report"]
            )

    return report_message, report_dict["Paths"]["zip"]


def idq_data_clean(report_dict):
    '''
    Clean Idq measured data.
    '''
    idq_clean = {
        "Interpolated Values":None,
        "Torque Averaged":{
            "Torque Measured mean (Nm)":None,
            "psi m":None,
        },
        "45":{
            "Ld - Lq":None,
            "psi m interp":None,
        },
        "0":None
    }

    report_dict["Data"].update(idq_clean)

    if ((report_dict["Data"]["Measured"]["Iq Current Injected"].to_numpy() > 0).any()
        and (report_dict["Data"]["Measured"]["Iq Current Injected"].to_numpy() < 0).any()):

        # Split motoring and generating quadrants
        df_motoring = report_dict["Data"]["Measured"].loc[(
            (report_dict["Data"]["Measured"]["Iq Current Injected"] > 0) &
            (report_dict["Data"]["Measured"]["Speed Measured [rpm]"] > 0)) | (
                (report_dict["Data"]["Measured"]["Iq Current Injected"] < 0) &
                (report_dict["Data"]["Measured"]["Speed Measured [rpm]"] < 0))]

        df_generating = report_dict["Data"]["Measured"].loc[(
            (report_dict["Data"]["Measured"]["Iq Current Injected"] < 0) &
            (report_dict["Data"]["Measured"]["Speed Measured [rpm]"] > 0)) | (
                (report_dict["Data"]["Measured"]["Iq Current Injected"] > 0) &
                (report_dict["Data"]["Measured"]["Speed Measured [rpm]"] < 0))]

        # Reset index so we can join
        df_motoring = df_motoring.reset_index()
        df_generating = df_generating.reset_index()
        
        # join two dataframe side by side - not keen on this method
        report_dict["Data"]["Torque Averaged"] = df_motoring.join(
            other=df_generating,
            lsuffix='_motoring',
            rsuffix='_generating'
        )
        
        # Find average of motoring and generating
        report_dict["Data"]["Torque Averaged"]["Torque Measured (mean) [Nm]"] = (abs(report_dict["Data"]["Torque Averaged"]["Torque Measured [Nm]_motoring"]
                                                        ) + abs(report_dict["Data"]["Torque Averaged"]["Torque Measured [Nm]_generating"])) / 2


        report_dict["Data"]["Torque Averaged"].columns = report_dict["Data"]["Torque Averaged"].columns.str.replace(
            pat="_motoring",
            repl=""
        )

        report_dict["Data"]["Torque Averaged"].to_csv(
            path_or_buf=report_dict["Paths"]["Torque Averaged"],
            header=True,
            index=False
        )
        try:
            # Calculate psi m of the motor from the results where the angle is 0.
            report_dict["Data"]["Torque Averaged"]["psi m"] = report_dict["Data"]["Torque Averaged"]["Torque Measured (mean) [Nm]"] /  (3 * int(6) * report_dict["Data"]["Torque Averaged"]["Iq Current Injected"])

            # Find 45 deg and 0 deg.
            report_dict["Data"]["45"]  = report_dict["Data"]["Torque Averaged"].loc[abs(( report_dict["Data"]["Torque Averaged"]["Is Current Injected Angle [Degrees]"]) == 45)]

            report_dict["Data"]["0"] = report_dict["Data"]["Torque Averaged"].loc[abs( report_dict["Data"]["Torque Averaged"]["Is Current Injected Angle [Degrees]"] == 0)]

            report_dict["Data"]["0"] = report_dict["Data"]["0"].loc[abs(report_dict["Data"]["0"]["Iq Current Injected"] > 0)]

            # Calculate the interpolation function, psi = function(iq)
            psi_m_f = interpolate.interp1d(
                x=report_dict["Data"]["0"]["Iq Current Injected"].to_numpy(),
                y=report_dict["Data"]["0"]["psi m"].to_numpy(),
                kind="cubic"
            )

            # For plotting psi m , create a linearly spaced array from min max with even spaced numbers.
            report_dict["Data"]["Interpolated Values"] = psi_m_f(
                np.linspace(
                    start=min(report_dict["Data"]["0"]["Iq Current Injected"]),
                    stop=max(report_dict["Data"]["0"]["Iq Current Injected"]),
                    num=50
                )
            )

            # Use the same function but with the 45 degree results.
            report_dict["Data"]["45"]["psi m interp"] = psi_m_f(report_dict["Data"]["45"]["Iq Current Injected"])
            
            # Calculate Ld - LQ
            report_dict["Data"]["45"]["Ld - Lq"] = (
                (((4 / 2) * int(6) * report_dict["Data"]["45"]["Torque Measured [Nm]"]) - (report_dict["Data"]["45"]["psi m interp"] *
                report_dict["Data"]["45"]["Iq Current Injected"] * np.sqrt(2))) / (
                    report_dict["Data"]["45"]["Iq Current Injected"] * np.sqrt(2) *
                    report_dict["Data"]["45"]["Id Current Injected"] * np.sqrt(2))
            )
            
            report_dict["Data"]["45"].to_csv(
                path_or_buf=report_dict["Paths"]["45"],
                header=True,
                index=False
            )

            report_dict["Data"]["0"].to_csv(
                path_or_buf=report_dict["Paths"]["0"],
                header=True,
                index=False
            )

        except Exception as e:
            print(e)

    return report_dict


def idq_characterisation_plots(report_dict):
    '''
    Setup Inputs to the idq characterisation report
    '''

    idq_plot = plot.surface_idq(
        df_all=report_dict["Data"]["Torque Averaged"],
        df_45=report_dict["Data"]["45"],
        df_0=report_dict["Data"]["0"]
    )

    psi_m = plot.psi_m(
        df_0= report_dict["Data"]["0"],
        interp_vals= report_dict["Data"]["Interpolated Values"]
    )

    ld_minus_lq = plot.ld_minus_lq(
        df_45 = report_dict["Data"]["45"]
    )

    current_sensor = plot.current_sensor_latency(
        data = report_dict["Data"]["Current Sensor"]
    )

    measured_plots = {
        "Measured":{
            "Idq" : idq_plot,
            "Psi M" : psi_m,
            "Ld Minus Lq" : ld_minus_lq,
            "Current Sensor" : current_sensor
        }
    }

    report_dict["Plots"].update(measured_plots)

    return report_dict


def torque_speed_sweep_plots(report_dict):
    '''
    Setup Inputs to the torque speed report
    '''
    print(report_dict["Data"]["Current Sensor"])
    nm_contour = plot.speed_torque_error_contour(
        df_all=report_dict["Data"]["Measured"],
        error_type="[Nm]"
    )

    nm_line = plot.speed_torque_error_line(
        df_all=report_dict["Data"]["Measured"],
        error_type="[Nm]"
    ) 

    pc_contour = plot.speed_torque_error_contour(
        df_all=report_dict["Data"]["Measured"],
        error_type="[%]"
    )

    pc_line = plot.speed_torque_error_line(
        df_all=report_dict["Data"]["Measured"],
        error_type="[%]"
    )

    current_sensor = plot.current_sensor_latency(
        data= report_dict["Data"]["Current Sensor"],
    )
    
    measured_plots = {
        "Measured": {
            "Nm Contour": nm_contour,
            "Nm Line": nm_line,
            "% Contour": pc_contour,
            "% Line": pc_line,
            "Current Sensor": current_sensor
            }
        }

    report_dict["Plots"].update(measured_plots)

    return report_dict

def read_inverter_properties(path, dict):
    '''
    Get the inverter properiy from path, if does not exsists, fill.
    '''

    if exists(path):
        data = pd.read_csv(path)
        data = data.squeeze()

    else:
        data = pd.Series(dict)
    
    data = pd.DataFrame(data)

    return data


def csv_to_dict(path, dict):
    '''
    Get the I sense table from path, if does not exsists, fill.
    '''
    if exists(path):

        data = pd.read_csv(path)
        
    else:

        data = [dict]
        
    data = pd.DataFrame(data)

    return data


def zip_report(report_path, zip_path):
    '''
    Add report to compressed archive on the input path.
    '''
    try:

        with zipfile.ZipFile(
            file = zip_path,
            mode="w",
            compression=zipfile.ZIP_DEFLATED,
            compresslevel=6
        ) as archive:

            archive.write(
                filename=report_path,
                arcname=report_path
            )   
            
            archive.close() 
        
        zipped = True

        message = f"Report compressed within a .zip archive: {zip_path}"

        print(message)

    except Exception as e:
        zipped = False
        message = "Could not Zip file" + str(e)

    return zipped, message 


def delete_report(report_path):
    '''
    '''
    try:
        os.remove(report_path)
        print("Report Deleted")    

    except Exception as e:
        print(e)
        print("Could not Delete Report")


def move_report(zip_path, remote_path):
    '''
    '''
    try:
        shutil.move(
            src=zip_path,
            dst=remote_path
        )

        message = True   

    except Exception as e:
        message = (f"Could not move {zip_path} to {remote_path}")
        print(e)

    return message

def torque_speed_sweep_report(report_dict):
    '''
    '''
    blank = dp.Text(".")

    config_table = {
        "Test Name":report_dict["Config"]["Name"] ,
        "Test Profile": str(report_dict["Config"]["Test"]["Type"] + ": " + report_dict["Config"]["Test"]["Profile"] ),
        "MCU Project": report_dict["Config"]["MCU"]["Name"],
        "MCU Sample": str(report_dict["Config"]["MCU"]["Sample Letter"] + report_dict["Config"]["MCU"]["Sample Number"]),
        "MCU Note": report_dict["Config"]["MCU"]["Note"],
        "MCU Current Limits": (str(report_dict["Config"]["MCU"]["Discharge Limit"]) + " | " + str(report_dict["Config"]["MCU"]["Charge Limit"])),
        "Motor Manfacturer": report_dict["Config"]["Motor"]["Manufacturer"],
        "Motor Sample": str(report_dict["Config"]["Motor"]["Sample Letter"] + report_dict["Config"]["MCU"]["Sample Number"]),
        "Motor Note": report_dict["Config"]["Motor"]["Note"],
        "Dyno": report_dict["Config"]["Dyno"]["Location"],
        "Dyno Gear Ratio": report_dict["Config"]["Dyno"]["Gearbox Ratio"],
        "DCDC Voltage Target": report_dict["Config"]["DCDC"]["Voltage"],
        "DCDC Current Limits": (str(report_dict["Config"]["DCDC"]["Current +"]) + " | " + str(report_dict["Config"]["DCDC"]["Current -"])),
        "CAN MCU": report_dict["Config"]["CAN"]["MCU CAN HWID"],
        "CAN DCDC": report_dict["Config"]["CAN"]["DCDC CAN HWID"],
        "Temperature Dwell": report_dict["Config"]["Temp"]["Enable"],
        "Temperature Upper Limit": report_dict["Config"]["Temp"]["Limit Upper"],
        "Temperature Lower Limit": report_dict["Config"]["Temp"]["Limit Lower"],
        "Logging on Ramp Up": report_dict["Config"]["Logging"]["Up"],
        "Logging on Ramp Down": report_dict["Config"]["Logging"]["Down"],
        "Logging Sample Period": report_dict["Config"]["Logging"]["Sample Period"],
        "Logging Delay": report_dict["Config"]["Logging"]["Delay"],
        "Logging Delay Period": report_dict["Config"]["Logging"]["Delay Period"]
    }

    page_home = dp.Page(title="Configuration",blocks=[
        dp.Group(blank,dp.Media(file=report_dict["Paths"]["logo"], name="Logo"),blank,columns=3),
        dp.Text("# Torque Speed Sweep Report"),
        dp.Group(
            dp.BigNumber(
                heading="Logged Points", 
                value=len(report_dict["Data"]["Input"])
                ),
            dp.BigNumber(
                heading="Average Motor Temperature", 
                value= f"{np.mean(report_dict['Data']['Measured']['Motor Stator Temperature [C]']):.3f}°C"
                ),
            dp.BigNumber(
                heading="Average Inverter Temperature", 
                value = f"{np.mean(report_dict['Data']['Measured']['Inverter Temperature [C]']):.3f}°C"
                ),
            dp.BigNumber(
                heading="Average Torque Error Nm", 
                value = f"{np.mean(report_dict['Data']['Measured']['Torque Demanded Actual Error [Nm]']):.3f} Nm"
                ),
            dp.BigNumber(
                heading="Average Torque Error %", 
                value = f"{np.mean(report_dict['Data']['Measured']['Torque Demanded Actual Error [%]']):.3f} %"
                ),
            columns=5
        ),
        dp.Text("## Configurations"),
        dp.Select(blocks=[
            dp.Table(
                data = pd.DataFrame(config_table.values(), index = config_table.keys()),
                caption = "Test Configuration Table",
                label="Test"
                ),
            dp.Table(
                data = pd.DataFrame(report_dict["Data"]["Inverter Properties"]),
                caption = "Software Properties read from the inverter.",
                label = "Software"
                ),
            dp.Group(
                dp.Table(
                    data = pd.DataFrame(report_dict["Data"]["Current Sensor"]),
                    caption = "Current Sensor Latency Table"
                ),
                dp.Plot(
                    data = report_dict["Plots"]["Measured"]["Current Sensor"],
                    caption = "Current Sensor Latency Plot" 
                ),
                columns = 2,
                label = "Current Sensor Latency"),
            ]),
        ])
    
    page_input = dp.Page(title="Input Data", blocks=[
        dp.DataTable(
            df = report_dict["Data"]["Input"],
            caption = "Input Data"
            ),
        dp.Select(
            blocks=[
                dp.Plot(data = report_dict["Plots"]["Input"]["Scatter"], label= "Scatter"),
                dp.Plot(data = report_dict["Plots"]["Input"]["Line"], label= "Timeline")
            ]
            ),
    ])

    page_results = dp.Page(title="Results Data", blocks=[
        dp.Text("The Table below represets the input, feedback and calcualted data during the torque speed sweep for quadrants Q"),
        dp.Text("Torque error [Nm] was calculated using the below equation : ") ,
        dp.Formula(
                formula=r"TorqueDemandActualError_{Nm} = TorqueMeasured_{Nm} - TorqueDemandedActual_{Nm}",
                caption="Torque Demand Actual Error in Netwon Meters"
                ),
        dp.Text("The Torque error percentage [%] was calculated using the below equation : "),
        dp.Formula(
                formula=r"TorqueDeamandActualError_{%} = \frac{TorqueDemandActualError_{Nm}}{TorqueMeasured_{Nm}} 100",
                caption="Torque Demand Actual Error in percentage"
                ),
        dp.DataTable(
                df = report_dict["Data"]["Measured"],
                caption="Torque Speed Sweep Results"
            )
    ])

    page_plots = dp.Page(title="Results Plots", blocks=[
    dp.Text("## Torque Error [Nm] Plots"),
    dp.Select(
            blocks=[
                dp.Plot(data =  report_dict["Plots"]["Measured"]["Nm Contour"], label= "Contour"),
                dp.Plot(data = report_dict["Plots"]["Measured"]["Nm Line"], label= "Line")
            ]
            ),

    dp.Text("## Torque Error [%] Plots"),
    dp.Select(
            blocks=[
                dp.Plot(data = report_dict["Plots"]["Measured"]["% Contour"], label= "Contour"),
                dp.Plot(data = report_dict["Plots"]["Measured"]["% Line"], label= "Line")
            ]
            )
    ])

    page_attachment = dp.Page(title="Attachments", blocks=[
        dp.Text("Result Data"),
        dp.Attachment(file=report_dict["Paths"]["measured"]),
        dp.Text("Test Script"),
        dp.Attachment(file=report_dict["Paths"]["script"]),
        dp.Text("Script Log"),
        dp.Attachment(file = report_dict["Paths"]["log"]),
        dp.Text("Alert Log"),
        dp.Attachment(file=report_dict["Paths"]["Alert"]),
    ])
    
    report = dp.Report(
        page_home,
        page_input,
        page_results,
        page_plots,
        page_attachment,
        layout=dp.PageLayout.SIDE
    )

    return report


def idq_characterisation_report(report_dict):
    '''

    '''
    blank = dp.Text(".")

    config_table = {
        "Test Name":report_dict["Config"]["Name"] ,
        "Test Profile": str(report_dict["Config"]["Test"]["Type"] + ": " + report_dict["Config"]["Test"]["Profile"] ),
        "MCU Project": report_dict["Config"]["MCU"]["Name"],
        "MCU Sample": str(report_dict["Config"]["MCU"]["Sample Letter"] + report_dict["Config"]["MCU"]["Sample Number"]),
        "MCU Note": report_dict["Config"]["MCU"]["Note"],
        "MCU Current Limits": (str(report_dict["Config"]["MCU"]["Discharge Limit"]) + " | " + str(report_dict["Config"]["MCU"]["Charge Limit"])),
        "Motor Manfacturer": report_dict["Config"]["Motor"]["Manufacturer"],
        "Motor Sample": str(report_dict["Config"]["Motor"]["Sample Letter"] + report_dict["Config"]["MCU"]["Sample Number"]),
        "Motor Note": report_dict["Config"]["Motor"]["Note"],
        "Dyno": report_dict["Config"]["Dyno"]["Location"],
        "Dyno Gear Ratio": report_dict["Config"]["Dyno"]["Gearbox Ratio"],
        "DCDC Voltage Target": report_dict["Config"]["DCDC"]["Voltage"],
        "DCDC Current Limits": (str(report_dict["Config"]["DCDC"]["Current +"]) + " | " + str(report_dict["Config"]["DCDC"]["Current -"])),
        "CAN MCU": report_dict["Config"]["CAN"]["MCU CAN HWID"],
        "CAN DCDC": report_dict["Config"]["CAN"]["DCDC CAN HWID"],
        "Temperature Dwell": report_dict["Config"]["Temp"]["Enable"],
        "Temperature Upper Limit": report_dict["Config"]["Temp"]["Limit Upper"],
        "Temperature Lower Limit": report_dict["Config"]["Temp"]["Limit Lower"],
        "Logging on Ramp Up": report_dict["Config"]["Logging"]["Up"],
        "Logging on Ramp Down": report_dict["Config"]["Logging"]["Down"],
        "Logging Sample Period": report_dict["Config"]["Logging"]["Sample Period"],
        "Logging Delay": report_dict["Config"]["Logging"]["Delay"],
        "Logging Delay Period": report_dict["Config"]["Logging"]["Delay Period"]
    }

    page_home = dp.Page(title="Configuration",blocks=[
        dp.Group(blank,dp.Media(file=report_dict["Paths"]["logo"], name="Logo"),blank,columns=3),
        dp.Text("# Idq Characterisation Report"),
        dp.Group(
            dp.BigNumber(
                heading="Logged Points", 
                value=len(report_dict["Data"]["Input"])
                ),
            dp.BigNumber(
                heading="Average Motor Temperature", 
                value= f"{np.mean(report_dict['Data']['Measured']['Motor Stator Temperature [C]']):.3f}°C"
                ),
            dp.BigNumber(
                heading="Average Inverter Temperature", 
                value = f"{np.mean(report_dict['Data']['Measured']['Inverter Temperature [C]']):.3f}°C"
                ),
            columns=3
        ),
        dp.Text("## Configurations"),
        dp.Select(blocks=[
            dp.Table(
                data = pd.DataFrame(pd.DataFrame(config_table.values(), index = config_table.keys())),
                caption = "Test environment configuration data",
                label="Environment"
                ),
            dp.Table(
                data = pd.DataFrame(report_dict["Data"]["Inverter Properties"]),
                caption = "Software Properties read from the inverter.",
                label = "Software"
                ),
            dp.Group(
                dp.Table(
                    data = pd.DataFrame(report_dict["Data"]["Current Sensor"]),
                    caption = "Current Sensor Latency Table"
                ),
                dp.Plot(
                    data = report_dict["Plots"]["Measured"]["Current Sensor"],
                    caption = "Current Sensor Latency Plot" 
                ),
                columns = 2,
                label = "Current Sensor Latency"),
            ]),
        ])

    page_input = dp.Page(title="Input Data", blocks=[
        dp.DataTable(
            df = report_dict["Data"]["Input"],
            caption = "Input Data"
            ),
        dp.Select(
            blocks=[
                dp.Plot(data = report_dict["Plots"]["Input"]["Scatter"], label= "Points"),
                dp.Plot(data = report_dict["Plots"]["Input"]["Line"], label= "Timeline")
            ]
            ),
    ])

    page_results = dp.Page(title="Results Data", blocks=[
        dp.Text("The Table below represets the input, feedback and calculated data during the Idq Injection for operating profile: " + str(report_dict["Config"]["Test"]["Profile"])),
        dp.Text("Psi_m was calculated using the below equation : ") ,
        dp.Formula(
            formula = r"\frac{TorqueMeasured_{Mean Nm}}{3 P_p I_{q Injected}}",
            caption = "Equation to calculate Psi_m"
        ),
        dp.Text("Ld - Lq was calculated using the below equation : "),
        dp.Formula(        
            formula = r"L_{d} - L_{q} = \frac{ (\frac{4}{2} P_p TorqueMeasured_{Nm}) - (\Psi Iq_{45\circ} \sqrt2)} { Iq_{45\circ} \sqrt2 Id_{45\circ} \sqrt2 }",
            caption = "Equation to calculate Ld - Lq"
        ),
        dp.DataTable(
                df = report_dict["Data"]["Measured"],
                caption="Idq Characterisation Results"
            )
    ])

    page_plots = dp.Page(title="Results Plots", blocks=[
        dp.Text("## Idq Plot"),
        dp.Plot(
            data = report_dict["Plots"]["Measured"]["Idq"],
            caption= "Surface Plot of Idq Injected Values and the resulting measured Torque"
        ),
        dp.Text("## Psi M Plot"),
        dp.Plot(
            data = report_dict["Plots"]["Measured"]["Psi M"],
            caption="Line Plot of calculated Psi_m and interpolated values"
        ),
        dp.Text("## Ld - Lq Plot"),
        dp.Plot(
            data = report_dict["Plots"]["Measured"]["Ld Minus Lq"],
            caption = "Line Plot of calculated Ld - Lq"
            )
    ])

    page_attachment = dp.Page(title="Attachments", blocks=[
        dp.Text("Result Data"),
        dp.Attachment(file=report_dict["Paths"]["measured"]),
        dp.Text("Result Data: Torque Averaged"),
        dp.Attachment(file=report_dict["Paths"]["Torque Averaged"]),
        dp.Text("Result Data: Is Angle = 45"),
        dp.Attachment(file=report_dict["Paths"]["45"]),
        dp.Text("Result Data: Is Angle = 0"),
        dp.Attachment(file=report_dict["Paths"]["0"]),
        dp.Text("Test Script"),
        dp.Attachment(file=report_dict["Paths"]["script"]),
        dp.Text("Script Log"),
        dp.Attachment(file=report_dict["Paths"]["log"]),
        dp.Text("Alert Log"),
        dp.Attachment(file=report_dict["Paths"]["Alert"]),
    ])

    report = dp.Report(
        page_home,
        page_input,
        page_results,
        page_plots,
        page_attachment,
        layout=dp.PageLayout.SIDE
    )

    return report


