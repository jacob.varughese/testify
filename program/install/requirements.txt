datapane>=0.15.0
numpy>=1.23.2
pandas>=1.4.3
Pillow>=9.2.0
plotly>=5.10.0
pyserial>=3.5
scipy>=1.9.0
streamlit>=1.12.0